#!/bin/bash

BODY_FILE=$(mktemp)

BUILD_NAME=${JOB:-iob-tp1}
BUILD_ID=1358524
STATUS=${STATUS:-failed}

cat > ${BODY_FILE} <<EOF
{
  "object_kind": "build",
  "ref": "master",
  "tag": false,
  "before_sha": "e1bbda4383a8aadcaa5e0c294a6cd269b16facf8",
  "sha": "624b617434a543ea672fd6955aab44c27fb0521a",
  "build_id": ${BUILD_ID},
  "build_name": "${BUILD_NAME}",
  "build_stage": "stage4",
  "build_status": "${STATUS}",
  "build_started_at": "2020-11-05 17:36:20 +0100",
  "build_finished_at": null,
  "build_duration": 0.11877697,
  "build_allow_failure": false,
  "build_failure_reason": "unknown_failure",
  "pipeline_id": 353932,
  "runner": {
    "id": 3283,
    "description": "gitlab-runner-gitlab-runner-5bf4b96cdc-v8bjk",
    "active": true,
    "is_shared": false
  },
  "project_id": 42490,
  "project_name": "Jupyter / Profile Definitions",
  "user": {
    "name": "Steffen Vogel",
    "username": "stvogel",
    "avatar_url": "https://git.rwth-aachen.de/uploads/-/system/user/avatar/255/avatar.png",
    "email": "stvogel@eonerc.rwth-aachen.de"
  },
  "commit": {
    "id": 353932,
    "sha": "624b617434a543ea672fd6955aab44c27fb0521a",
    "message": "fix slug filename mismatch",
    "author_name": "Steffen Vogel",
    "author_email": "stvogel@eonerc.rwth-aachen.de",
    "author_url": "https://git.rwth-aachen.de/stvogel",
    "status": "running",
    "duration": null,
    "started_at": "2020-11-05 17:36:00 +0100",
    "finished_at": null
  },
  "repository": {
    "name": "Profile Definitions",
    "url": "git@git.rwth-aachen.de:jupyter/profiles.git",
    "description": "Profile definitions YAMLs consumed by the Profile Builder",
    "homepage": "https://git.rwth-aachen.de/jupyter/profiles",
    "git_http_url": "https://git.rwth-aachen.de/jupyter/profiles.git",
    "git_ssh_url": "git@git.rwth-aachen.de:jupyter/profiles.git",
    "visibility_level": 10
  }
}
EOF

KCTL="kubectl --context=rwth-jupyter-rancher"
export GITLAB_WEBHOOK_TOKEN="$(${KCTL} -n jhub get secret service-token-profile -o jsonpath='{.data.gitlab-webhook-token}' | base64 -d)"

echo "=== Configuration"
echo "  GITLAB_WEBHOOK_TOKEN=${GITLAB_WEBHOOK_TOKEN}"

curl --verbose --data-binary @${BODY_FILE} \
    -H "Content-type: application/json" \
    -H "x-gitlab-event: Job Hook" \
    -H "x-gitlab-token: ${GITLAB_WEBHOOK_TOKEN}" \
    -H "x-request-id: 79cb404b-10dd-4f92-9946-8d8313451e2c" \
    -H "x-forwarded-for: 134.130.122.52" \
    http://localhost:5000/services/profile/api/hook | jq .