import os
import re
import pkg_resources
import logging
import base64
import json
import datetime
import tornado
import hashlib

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from contextlib import contextmanager

from traitlets.config.application import Application
from traitlets import List, Dict, Bool, Unicode, default

from jinja2 import Environment, FileSystemLoader, PrefixLoader, ChoiceLoader
from aioredis import Redis

from urllib.parse import urlparse, urlencode

from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.web import StaticFileHandler, RequestHandler
from tornado.web import Application as TornadoApplication

from jupyterhub.services.auth import HubOAuthCallbackHandler
from jupyterhub.utils import url_path_join

from jupyterhub_services.handlers.healthz import HealthzCallbackHandler
from jupyterhub_services.handlers.base import BaseRequestHandler
from jupyterhub_services.orm import Base
from jupyterhub_services.util import RedisCache


def _filter_json(data, indent=None):
    return json.dumps(data, indent=indent, cls=JSONDateTimeEncoder)


def _filter_b64encode(data):
    return base64.b64encode(data.encode('utf-8')).decode('utf-8')

class JSONDateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return obj.isoformat()


class Service(Application):

    # Service
    prefix = Unicode()
    url = Unicode()
    host = Unicode()
    token = Unicode()
    moodle_token = Unicode()
    moodle_url = Unicode('https://moodle.rwth-aachen.de')
    public_url = Unicode('https://jupyter.rwth-aachen.de')
    sciebo_client_id = Unicode()
    sciebo_client_secret = Unicode()

    # Templating
    jinja_env_options = Dict(
        help="Supply extra arguments that will be passed to Jinja environment."
    ).tag(config=True)

    template_paths = List(
        help="Paths to search for jinja templates,"
             "before using the default templates."
    ).tag(config=True)

    @default('template_paths')
    def _template_paths_default(self):
        return [pkg_resources.resource_filename(__name__, 'templates/')]

    template_vars = Dict(help="Extra variables to be "
                              "passed into jinja templates").tag(
        config=True
    )

    # Database
    db_url = Unicode()
    db_reset = Bool(False)

    # Cache
    redis_url = Unicode()

    debug = Bool(True)

    @default('token')
    def _default_token(self):
        return os.environ['JUPYTERHUB_API_TOKEN']

    @default('moodle_token')
    def _default_moodle_token(self):
        return os.environ['MOODLE_TOKEN']

    @default('sciebo_client_id')
    def _default_sciebo_client_id(self):
        return os.environ['SCIEBO_CLIENT_ID']

    @default('sciebo_client_secret')
    def _default_sciebo_client_secret(self):
        return os.environ['SCIEBO_CLIENT_SECRET']

    @default('debug')
    def _default_debug(self):
        return os.environ.get('DEBUG', 'false') == 'true'

    @default('host')
    def _default_host(self):
        return os.environ.get('JUPYTERHUB_HOST',
                              'http://localhost')

    @default('prefix')
    def _default_prefix(self):
        return os.environ.get('JUPYTERHUB_SERVICE_PREFIX',
                              f'/services/{self.name}/')

    @default('url')
    def _default_url(self):
        return os.environ.get('JUPYTERHUB_SERVICE_URL',
                              'http://[::]:5000')

    @default('db_url')
    def _default_db_url(self):
        return os.environ.get('DB_URL')

    @default('redis_url')
    def _default_redis_url(self):
        return os.environ.get('REDIS_URL')

    def __init__(self):
        logging.basicConfig(level=logging.DEBUG if self.debug else logging.INFO)
        self.log = logging.getLogger('tornado.application')

        # Set some env vars for JupyterHub's HubOAuth
        if 'JUPYTERHUB_SERVICE_PREFIX' not in os.environ:
            os.environ['JUPYTERHUB_SERVICE_PREFIX'] = f'/services/{self.name}'

        if 'JUPYTERHUB_CLIENT_ID' not in os.environ:
            os.environ['JUPYTERHUB_CLIENT_ID'] = self.name

        self._init_db()
        self._init_jinja()
        self._init_tornado()

    async def init(self):
        await self._init_redis()

    async def _init_redis(self):
        self.redis = await Redis.from_url(self.redis_url,
                                          max_connections=10)
        self.cache = RedisCache(self.redis)

    def _init_db(self):
        """Create a new session at url"""

        engine = create_engine(self.db_url)

        if self.db_reset:
            Base.metadata.drop_all(engine)

        Base.metadata.create_all(engine)

        self.db = sessionmaker(bind=engine)

    def _init_jinja(self):
        jinja_options = {
            'autoescape': True,
            'extensions': ['jinja2.ext.do'],
            **self.jinja_env_options
        }

        base_path = self._template_paths_default()[0]

        if base_path not in self.template_paths:
            self.template_paths.append(base_path)

        loader = ChoiceLoader([
            PrefixLoader({'templates': FileSystemLoader([base_path])}, '/'),
            FileSystemLoader(self.template_paths),
        ])

        self.jinja_env = Environment(loader=loader, **jinja_options)

        self.jinja_env.filters['b64encode'] = _filter_b64encode
        self.jinja_env.filters['tojson'] = _filter_json
        self.jinja_env.filters['toquerystring'] = urlencode
        self.jinja_env.filters['regex_replace'] = lambda t, p, r: re.sub(p, r, t)
        self.jinja_env.filters['md5'] = lambda x: hashlib.md5(x.encode('ascii')).hexdigest()

    def _init_tornado(self):

        settings = {
            'cookie_secret': os.urandom(32),
            'debug': self.debug,
            'static_path': pkg_resources.resource_filename(__name__,
                                                           'static/')
        }

        opts = {
            'service': self
        }

        handlers = [
            (url_path_join(self.prefix, r'oauth_callback'),
                HubOAuthCallbackHandler),
            (url_path_join(r'/healthz'),
                HealthzCallbackHandler)
        ]

        for handler in self.handlers:
            handler_url = url_path_join(self.prefix, handler[0])
            handler_class = handler[1]

            if issubclass(handler_class, BaseRequestHandler):
                handler_opts = opts.copy()
            else:
                handler_opts = {}

            if len(handler) > 2:
                handler_opts.update(handler[2])

            handlers.append((
                handler_url,
                handler_class,
                handler_opts
            ))

        handlers += [
            (url_path_join(self.prefix, r'(.*)'),
                StaticFileHandler, {
                    'path': settings['static_path']
                })
        ]

        tornado.escape.json_encode = _filter_json

        self.app = TornadoApplication(handlers, **settings)

    @contextmanager
    def session(self):
        """Provide a transactional scope around a series of operations."""
        session = self.db()
        try:
            yield session
            session.commit()
        except:
            session.rollback()
            raise
        finally:
            session.close()

    def start(self):
        url = urlparse(self.url)
        server = HTTPServer(self.app)
        server.listen(url.port, url.hostname)

        self.log.info(f'Listening on {url.hostname}:{url.port}')

        iol = IOLoop.current()
        iol.run_sync(self.init)
        iol.start()
