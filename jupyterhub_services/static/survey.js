const activeSurveyId = 2;
const activeSurveyUrl = 'https://www.global-assess.rwth-aachen.de/jupyter_eval/frontend/www/index.php?r=uberTest/createFreeAccessUser&id=' + surveyId;

const surveyModal = `
<div class="modal" tabindex="-1" role="dialog">
	<div id="survey-notice" class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Dear Users of RWTHjupyter,</h4>
			</div>
			<div class="modal-body">
				<img style="width: 150px; float: right;" src="{prefix}survey/survey_icon.png" />
				<p>Since the beginning of the current summer semester you had access to the interactive computing platform JupyterHub. During your course you could get to know the platform in the past months and gain versatile experience in using it.</p><br
				/>
				<p style="font-weight: bold">To further improve the this platform, we would like you to share your experiences using a online questionaire.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">Start the evaluation!</button>
				<button type="button" class="btn btn-secondary">I will do it later..</button>
			</div>
		</div>
	</div>
</div>`;

function showSurveyModal(surveyUrl) {
	// Inject modal dialog
	document.body.innerHTML += surveyModal;

	// Show modal dialog
	$("#survey-notice").modal('show');

	let visitBtn = document.querySelector('#survey-notice button.btn-primary');
	let closeBtn = document.querySelector('#survey-notice button.btn-secondary');

	visitBtn.addEventListener('click', () => { window.location.replace(surveyUrl); });
	closeBtn.addEventListener('click', () => {
		window.location.replace(surveyUrl);
		
	});

	$("#survey-notice button.btn-secondary").click(() => {
		$("#survey-notice").modal('hide');
		$(".modal").css('display', 'none')
	});

	// some hack...
	$(".modal").css('display', 'block')
}

async function loaded() {
	// JupyterHub 0.8 applied form-control indisciminately to all form elements.
	// Can be removed once we stop supporting JupyterHub 0.8
	for (elm of document.querySelectorAll('#kubespawner-profiles-list input[type="radio"]'))
		elm.classList.remove('form-control')

	// Check if user has completed survey, otherwise show modal
	data = await apiRequest('GET', '/survey');
	if (data["surveys"]) {
		surveysCompleted = {};

		for (survey of data['surveys']) {
			var id = survey['id'];

			if (id in surveysCompleted)
				surveysCompleted[id]++;
			else
				surveysCompleted[id] = 1;
		}

		if (!(activeSurveyId in surveysCompleted)) {
			showSurveyModal(activeSurveyUrl);
		}
	}
}

window.addEventListener('load', loaded);