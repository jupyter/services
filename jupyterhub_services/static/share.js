const types = {
	'pvc': 'Volume',
	'sciebo': 'Sciebo',
	'webdav': 'WebDAV'
};

function populateShareTable(shares) {
	let table = document.getElementById('shares');
	let tbody = table.querySelector('tbody');

	tbody.innerHTML = ''; /* Clear table */

	for (const share of shares) {
		let row = tbody.insertRow(-1);
		row.classList.add(share.severity);

		let noCell = row.insertCell();
		noCell.innerHTML = share.id;

		let nameCell = row.insertCell();
		nameCell.innerHTML = share.name;

		let mountpointCell = row.insertCell();
		mountpointCell.innerHTML = share.mountpoint;

		let typeCell = row.insertCell();
		typeCell.innerHTML = types[share.type];

		let userCell = row.insertCell();
		userCell.innerHTML = share.config.username || '';

		let serverCell = row.insertCell();
		serverCell.innerHTML = share.config.server ? '<a href="' + share.config.server + '">' + share.config.server + '</a>' : '';

		let quotaCell = row.insertCell();
		quotaCell.id = 'quota-' + share.id;
		quotaCell.innerHTML = `
			<div class="progress">
				<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
					loading
				</div>
			</div>`;

		let ctlCell = row.insertCell();
		ctlCell.innerHTML = '<a title="Delete" onclick="deleteShare(' + share.id + ')"><i aria-hidden="true" class="action fa fa-trash"></i></a>' +
		                    '<a title="Open in JupyterLab" href="https://jupyter.rwth-aachen.de/user-redirect/lab/tree/shared/' + share.mountpoint + '"><img class="link-icon" src="' + prefix + '/jupyterlab_icon.png" /></a>';

		switch (share.type) {
			case 'sciebo':
				ctlCell.innerHTML += '<a title="Open in Sciebo" href="' + share.config.server + '/apps/files/?dir=/"><img class="link-icon" src="' + prefix + '/sciebo_icon.svg" /></a>';

			case 'webdav':
				let webDavUrl = share.config.server + share.config.share;
				webDavUrl = webDavUrl.replace('http', 'dav');
				webDavUrl = webDavUrl.replace('://', '://' + share.config.username + '@');
				ctlCell.innerHTML += '<a title="Open as WebDAV share" href="' + webDavUrl + '"><i aria-hidden="true" class="action fa fa-cloud"></i></a>';
				break;
		}
	}
}

async function updateShareQuota(share) {
	let resp = await apiRequest('GET', '/share/api/' + share.id + '/quota');

	if (!resp.quota)
		return;

	let quota = resp.quota;
	let quotaAvailableHuman = humanFileSize(quota.available);
	let quotaUsedHuman = humanFileSize(quota.used);
	let quotaPerc = Math.round(100 * quota.used / quota.available);

	let quotaCell = document.getElementById('quota-' + share.id);

	quotaCell.innerHTML = `
	<div class="progress">
		<div class="progress-bar" role="progressbar" aria-valuenow="${quotaPerc}" aria-valuemin="0" aria-valuemax="100" style="width: ${quotaPerc}%">
			${quotaPerc}%
		</div>
	</div>
	<span>${quotaUsedHuman} / ${quotaAvailableHuman}</span>`;
}

async function updateShareTable() {
	data = await apiRequest('GET', '/share/api');
	shares = data["shares"]
	if (!shares)
		return;

	populateShareTable(shares);

	for (share of shares)
		await updateShareQuota(share)
}

async function deleteShare(id) {
	await apiRequest('DELETE', '/share/api/' + id)
	await updateShareTable();
}

async function addShare(ev) {
	ev.preventDefault();

	let frm = ev.target;

	let type = frm.elements.type.value;
	let share = {
		type: type,
		mountpoint: frm.elements.mountpoint.value,
		name: frm.elements.name.value,
		config: {}
	}

	switch (type) {
		case 'pvc':
			share.config = {
				capacity: frm.elements.pvc_capacity.value
			}
			break;

		case 'webdav':
			share.config = {
				server: frm.elements.webdav_server.value,
				share: frm.elements.webdav_share.value,
				username: frm.elements.webdav_username.value,
				password: frm.elements.webdav_password.value
			};
			break;

		case 'sciebo':
			share.config = {
				server: frm.elements.sciebo_server.value,
				permissions: parseInt(frm.elements.sciebo_permissions.value),
				path: frm.elements.sciebo_path.value,
				user_id: frm.elements.sciebo_user_id.value,
			};
			break;
	}

	let scieboAccessToken = getCookie('sciebo_access_token')
	let scieboServer = getCookie('sciebo_server')

	let requiresScieboAuthorization = false;
	if (type == 'sciebo') {
		if (!scieboAccessToken || !scieboServer || scieboServer != share.config.server)
			requiresScieboAuthorization = true;
	}

	if (requiresScieboAuthorization) {
		scieboAuthorize(share.config.server, share);
	}
	else {
		await apiRequest('POST', '/share/api', share);
		await updateShareTable();
	}

	frm.reset(); // clear inputs
}

function updateFormVisibility() {
	let selectType = document.getElementById('type');
	let divWebDav = document.getElementById('add-share-form-webdav');
	let divSciebo = document.getElementById('add-share-form-sciebo');
	let divPvc = document.getElementById('add-share-form-pvc');

	let type = selectType.value;

	divPvc.classList.add('hidden');
	divWebDav.classList.add('hidden');
	divSciebo.classList.add('hidden');

	switch (type) {
		case 'pvc':
			divPvc.classList.remove('hidden');
			break;

		case 'webdav':
			divWebDav.classList.remove('hidden');
			break;

		case 'sciebo':
			divSciebo.classList.remove('hidden');
			break;
	}
}

function updateForm() {
	state_encoded = getQueryParameter('state');
	if (state_encoded) {
		state = JSON.parse(atob(state_encoded));

		let frm = document.getElementById('add-share-form');

		frm.elements.type.value = state.type;
		frm.elements.name.value = state.name;
		frm.elements.mountpoint.value = state.mountpoint;
		frm.elements.sciebo_server.value = state.config.server;
		frm.elements.sciebo_path.value = state.config.path;
		frm.elements.sciebo_user_id.value = state.config.user_id;
		frm.elements.sciebo_permissions.value = state.config.permissions;
	}

	updateFormVisibility();
}

async function updateScieboEndpoints() {
	let selScieboServer = document.getElementById('sciebo-server');
	let scieboEndpoints = await apiRequest('GET', '/share/api/sciebo/endpoints');

	for (ep of scieboEndpoints.endpoints) {
		let opt = document.createElement('option');

		opt.value=ep.url;
		opt.innerHTML = ep.name;

		selScieboServer.appendChild(opt)
	}
}

function scieboAuthorize(server, share) {
	window.location = server + '/index.php/apps/oauth2/authorize' + queryString({
		response_type: 'code',
			client_id: sciebo_client_id,
			redirect_uri: public_url + prefix + 'api/oauth_callback',
			state: btoa(JSON.stringify(share))
	});
}

async function loaded() {
	let frm = document.getElementById('add-share-form');

	frm.addEventListener('submit', addShare);
	frm.elements.type.addEventListener('change', updateFormVisibility)

	await updateScieboEndpoints();
	await updateShareTable();

	updateForm();
	setupFormValidation(frm);
}

window.addEventListener('load', loaded);