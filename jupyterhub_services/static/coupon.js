const apiBase = '/services/coupon/api';

async function createCoupon(ev) {
	ev.preventDefault();

	let frm = ev.target;
	let resultDiv = document.getElementById('create-result');

	let groups = $('#groups option:selected').map(function(_, item) { return item.value; }).get();
	let groups_new = frm.elements.groups_new.value;
	if (groups_new != '')
		groups = groups.concat(groups_new.split(','));

	body_params = {
		'available': frm.elements.available.value,
		'groups': groups.join(',')
	}

	let response = await apiRequest('POST', '/coupon/api/create', body_params)
	handleApiResponse(response, resultDiv, {
		success: 'Coupon created: <a href="' + response.redeem_url + '">' + response.redeem_url + '</a>'
	})
}

async function redeemCoupon(ev) {
	ev.preventDefault();

	let frm = ev.target;
	let coupon = frm.elements.coupon.value;
	let resultDiv = document.getElementById('redeem-result');

	try {
		let response = await apiRequest('GET', '/coupon/api/redeem/' + coupon);
		handleApiResponse(response, resultDiv, {
			success: 'Coupon successfully redeemed. You are now added to the groups ' + response.groups + '<br >' +
					'Remaining uses: ' + response.available + '<br />' +
					'Valid until: ' + response.valid_until
		})
	} catch (error) {
		console.log(error);
	}
}

function loaded() {
	$(document).ready(function() {
		$('#groups').multiselect({
			enableFiltering: true,
			buttonWidth: '100%',
		});

		let t = Date.now() + 3*30*24*60*60*1000; // 3 months in future
		let ts = new Date(t).toISOString().split("T")[0];

		$('#valid-until').val(ts);
	});

	let frmCreate = document.getElementById('create-coupon-form');
	frmCreate.addEventListener('submit', createCoupon);

	let frmRedeem = document.getElementById('redeem-coupon-form');
	frmRedeem.addEventListener('submit', redeemCoupon);

	let urlParams = new URLSearchParams(window.location.search);
	let coupon = urlParams.get('coupon');

	if (coupon) {
		let couponInput = document.getElementById('coupon');
		couponInput.value = coupon;
	}
}

window.addEventListener('load', loaded)