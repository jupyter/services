function clipSupportCode() {
	var copyText = document.getElementById('support-code');
	copyText.select();
	copyText.setSelectionRange(0, 99999); /* For mobile devices */
	document.execCommand('copy');
}

function loaded() {
	var copyText = document.getElementById('support-code');
	copyText.style.height = copyText.scrollHeight+'px'
}

window.addEventListener('load', loaded);