const serviceUrl = '/services';

function lightenDarkenColor(col, amt) {
	var usePound = false;
  
	if (col[0] == "#") {
		col = col.slice(1);
		usePound = true;
	}
 
	var num = parseInt(col,16);
	var r = (num >> 16) + amt;
 
	if (r > 255)
		r = 255;
	else if (r < 0)
		r = 0;
 
	var b = ((num >> 8) & 0x00FF) + amt;
	if (b > 255)
		b = 255;
	else if (b < 0)
		b = 0;
 
	var g = (num & 0x0000FF) + amt;
	if (g > 255)
		g = 255;
	else if (g < 0)
		g = 0;
 
	return (usePound?"#":"") + (g | (b << 8) | (r << 16)).toString(16);
}

function rgb2hex(rgb) {
	let rgbm = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
	
	return (rgbm && rgbm.length === 4) ? "#" +
		("0" + parseInt(rgbm[1], 10).toString(16)).slice(-2) +
		("0" + parseInt(rgbm[2], 10).toString(16)).slice(-2) +
		("0" + parseInt(rgbm[3], 10).toString(16)).slice(-2) : '';
}

// taken from https://stackoverflow.com/questions/6234773/can-i-escape-html-special-chars-in-javascript
function escapeHtml(unsafe) {
	return unsafe
		.replace(/&/g, "&amp;")
		.replace(/</g, "&lt;")
		.replace(/>/g, "&gt;")
		.replace(/"/g, "&quot;")
		.replace(/'/g, "&#039;");
}

function generateProfileDropdown(profiles, profilesSelect, filter=null) {
	for (var i = 0; i < profiles.length; i++) {
		var o = document.createElement("option");
		var profile = profiles[i];

		// skip profiles for which I dont own the manager role
		if (filter && !filter(profile))
			continue;

		o.value = profile.slug;
		o.textContent = profile.display_name;

		profilesSelect.appendChild(o);
	}
}

function getQueryParameter(name) {
	const params = new URLSearchParams(window.location.search);
	
	return params.get(name)
}

function getCookie(name) {
	const value = `; ${document.cookie}`;
	const parts = value.split(`; ${name}=`);
	
	if (parts.length === 2)
		return parts.pop().split(';').shift();
}

function queryString(params) {
	return '?' + Object.keys(params)
		.map(k => `${k}=${params[k]}`)
		.join('&');
}

async function apiRequest(method, endpoint, body, url) {
	let apiUrl = url ? url : serviceUrl;

	try {
		response = await fetch(apiUrl + endpoint, {
			'method': method,
			'mode': 'cors',
			'body': JSON.stringify(body),
			'init': {
				'credentials': 'include'
			}
		})

		return await response.json();
	}
	catch (error) {
		return {
			error: {
				code: -1,
				reason: error.toString()
			}
		}
	}	
}

function setupFormValidation(frm) {
	let elms = frm.querySelectorAll('input,select,textarea');
	for (elm of elms) {
		elm.addEventListener('input', (ev) => {
			let elm = ev.target;
			let fg = elm.closest('.form-group');
			let ok = elm.checkValidity();
			if (ok)
				fg.classList.remove('has-error');
			else
				fg.classList.add('has-error');
		});
		
		elm.dispatchEvent(new Event('input'));
	}

	frm.addEventListener('submit', (ev) => {
		ev.preventDefault();

		let frm = ev.target;
		let elms = frm.querySelectorAll('input,select,textarea');
		for (elm of elms) {
			let ok = elm.checkValidity();

			if (!ok) {
				elm.reportValidity();
				ev.stopPropagation();
			}
		}
	})
}

function handleApiResponse(response, elm, msg) {
	if (response.error) {
		if (msg.error === undefined)
			msg.error = 'Error ' + response.error.code + ': ' + response.error.reason;

		elm.innerHTML = msg.error
		elm.classList.add('alert-danger');
		elm.classList.remove('alert-success');
	}
	else {
		if (msg.success === undefined)
			msg.success = 'Success';

		elm.innerHTML = msg.success
		elm.classList.add('alert-success');
		elm.classList.remove('alert-danger');
	}
}

/**
 * Format bytes as human-readable text.
 * 
 * @param bytes Number of bytes.
 * @param si True to use metric (SI) units, aka powers of 1000. False to use 
 *           binary (IEC), aka powers of 1024.
 * @param dp Number of decimal places to display.
 * 
 * @return Formatted string.
 */
function humanFileSize(bytes, si=false, dp=1) {
  	 thresh = si ? 1000 : 1024;

	if (Math.abs(bytes) < thresh)
		return bytes + ' B';

	const units = si 
		? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'] 
		: ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
	let u = -1;
	const r = 10**dp;

	do {
		bytes /= thresh;
		++u;
	} while (Math.round(Math.abs(bytes) * r) / r >= thresh && u < units.length - 1);

	return bytes.toFixed(dp) + ' ' + units[u];
}

Number.prototype.pad = function(size) {
	var s = String(this);
	while (s.length < (size || 2)) {s = "0" + s;}
	return s;
}

Date.prototype.addDays = function(days) {
	var date = new Date(this.valueOf());
	date.setDate(date.getDate() + days);
	return date;
}

Date.prototype.toInputValue = function() {
	return this.getFullYear().pad(4) + '-' + (this.getMonth() + 1).pad(2) + '-' + this.getDate().pad(2) + 'T' + this.getHours().pad(2) + ':' + this.getMinutes().pad(2);
}