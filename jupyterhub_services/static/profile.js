async function triggerBuild(ev) {
	ev.preventDefault();

	let frm = ev.target;
	let slug = frm.elements.slug.value;

	let resultDiv = document.getElementById('profile-build-result');

	let reponse = await apiRequest('POST', '/profile/api/' + slug + '/build');

	handleApiResponse(reponse, resultDiv, {
		success: 'Pipeline created: <a href="' + reponse.web_url + '">' + reponse.web_url + '</a>'
	})
}

async function getSPDXLicenses() {
	let resp = await apiRequest('GET', '/profile/api/licenses/spdx');

	if (resp.licenses) {
		let licenseSelect = document.getElementById('license');

		let spdxOptgroup = document.createElement('optgroup');
		spdxOptgroup.label = 'SPDX';

		licenseSelect.insertAdjacentElement('beforeend', spdxOptgroup);

		for (let license in resp.licenses) {
			let opt = document.createElement('option');

			opt.value = license;
			opt.innerHTML = resp.licenses[license].name;

			spdxOptgroup.insertAdjacentElement('beforeend', opt);
		}
	}
}

async function loaded() {
	if (window.location.pathname.endsWith('/build')) {
		let profiles = await apiRequest('GET', '/profile/api/all');
		let user = await apiRequest('GET', '/whoami/api/me');

		let profileSelect = document.getElementById('slug');
		let buildForm = document.getElementById('build-form');

		let filter = (profile) => { return user.admin || user.groups.includes('profile-manager@' + profile.slug); };

		generateProfileDropdown(profiles['profiles'], profileSelect, filter);

		buildForm.addEventListener('submit', triggerBuild);
	}
	else if (window.location.pathname.endsWith('/create')) {
		await getSPDXLicenses();
	}
}

window.addEventListener('load', loaded);