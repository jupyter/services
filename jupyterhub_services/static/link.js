const spawnBaseUrl = "https://jupyter.rwth-aachen.de/hub/spawn";
const badgeURL = "https://jupyter.pages.rwth-aachen.de/documentation/images/badge-launch-rwth-jupyter.svg";

function updateLink(event) {
	var profileSlug = document.getElementById("link-wizard-profile");
	var filePath = document.getElementById("link-wizard-path");
	var outputDiv = document.getElementById("link-wizard-output");
	var directLink = document.getElementById("link-wizard-direct-link");
	var badgeDiv = document.getElementById("link-wizard-badge");
	var badgePre_md = document.getElementById("link-wizard-badge-md");
	var badgePreHtml = document.getElementById("link-wizard-badge-html");

	// Strip leading slash
	var path = filePath.value.replace(/^\//, '');
	var slug = profileSlug.value;

	if (slug) {
		var link = updateDirectLink(slug, path);

		// Badge Code
		var badge_md = '[![](' + badgeURL + ')](' + link + ')';
		var badgeHtml = '<a href="' + link + '"><img src="' + badgeURL + '" /></a>';

		badgePre_md.innerHTML = badge_md;
		badgePreHtml.innerHTML = escapeHtml(badgeHtml);

		// Link
		directLink.innerHTML = '<a href="' + link + '">' + link + '</a>';

		// Badge
		badgeDiv.innerHTML = badgeHtml

		outputDiv.style.display = 'block';
	}
	else {
		outputDiv.style.display = 'none';
	}
}

function updateDirectLink(profileSlug, path) {
	var url = spawnBaseUrl + '?profile=' + profileSlug;

	if (path && path.length > 0)
		url += '&next=/user-redirect/lab/tree/' + encodeURIComponent(path);

	return url;
}

async function loaded() {
	let profileSelect = document.getElementById('link-wizard-profile');
	let profiles = await apiRequest('GET', '/profile/api/all');
	let user = await apiRequest('GET', '/whoami/api/me');

	generateProfileDropdown(profiles['profiles'], profileSelect);
}

window.addEventListener('load', loaded);