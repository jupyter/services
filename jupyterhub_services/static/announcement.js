function makeAnnouncement(ann) {
	let annElm = document.createElement('div');

	annElm.innerHTML = originToName(ann.origin) + ': ' + ann.message;
	
	if (ann.class)
		annElm.classList.add(ann.class);
	
	if (ann.font_color) {
		annElm.style.color = ann.font_color;

		if (ann.origin == 'rwth-jupyter')
			annElm.style.borderColor = ann.font_color;
	}

	if (ann.background_color) {
		annElm.style.backgroundColor = ann.background_color;

		if (ann.origin == 'rwth-gitlab')
			annElm.style.borderColor = lightenDarkenColor(ann.background_color, -100);
	}

	if (ann.class)
		annElm.classList.add(ann.class)
	
	annElm.classList.add('announcement');

	return annElm;
}

async function fetchAnnouncements(all) {
	let anns = [];

	let data = await apiRequest('GET', '/announcement/api?all=' + (all ? 'true' : 'false'));
	if (data.announcements) {
		for (ann of data.announcements) {
			ann.show_after = new Date(ann.show_after);
			ann.show_until = new Date(ann.show_until);
			ann.timestamp = new Date(ann.timestamp);

			anns.push(ann);
		}
	}

	return anns;
}

async function loadAnnouncements() {
	let bar = document.querySelector('nav.navbar-default');

	// Clear old announcements
	let oldAnns = document.querySelectorAll('.announcement:not(.preview)');
	for (ann of oldAnns)
		ann.parentNode.removeChild(ann);

	let anns = await fetchAnnouncements(false);
	let elms = anns
				.sort((a, b) => a.show_after - b.show_after)
				.map(a => makeAnnouncement(a))
	
	for (elm of elms)
		bar.insertAdjacentElement('afterend', elm);
}

function originToName(origin) {
	switch (origin) {
		case 'rwth-gitlab':
			return 'RWTH GitLab';

		case 'rwth-jupyter':
			return 'RWTHjupyter';

		case 'rwth-itc-feed':
			return 'RWTH Störungsmeldungen';

		case 'preview':
			return 'Preview';
	}
}

function populateAnnouncementTable(announcements) {
	let table = document.getElementById('announcements');

	let tbody = table.querySelector('tbody');
	tbody.innerHTML = ''; /* Clear table */

	for (const ann of announcements) {
		let row = tbody.insertRow(-1);

		let idCell = row.insertCell();
		idCell.innerHTML = ann.id;

		let originCell = row.insertCell();
		originCell.innerHTML = originToName(ann.origin)

		if (ann.class)
			row.classList.add(ann.class);
		if (ann.font_color)
			row.style.color = ann.font_color;
		if (ann.background_color)
			row.style.backgroundColor = ann.background_color;

		let msgCell = row.insertCell();
		msgCell.innerHTML = ann.message;

		let sevCell = row.insertCell();
		sevCell.innerHTML = ann.severity || '';

		let afterCell = row.insertCell();
		afterCell.innerHTML = ann.show_after.toLocaleString();

		let untilCell = row.insertCell();
		untilCell.innerHTML = ann.show_until.toLocaleString();

		let userCell = row.insertCell();
		userCell.innerHTML = ann.user || '';

		let tsCell = row.insertCell();
		tsCell.innerHTML = ann.timestamp ? ann.timestamp.toLocaleString() : '';

		let ctlCell = row.insertCell();
		if (ann.origin == 'rwth-jupyter')
			ctlCell.innerHTML = '<a title="Delete" onclick="deleteAnnouncement(' + ann.id + ')"><i aria-hidden="true" class="fa fa-trash"></i></a>';
		else
			ctlCell.innerHTML = '';

	}
}

async function updateAnnouncementTable() {
	let anns = await fetchAnnouncements(true);
	anns = anns.sort((a, b) => b.show_after - a.show_after);

	populateAnnouncementTable(anns);
}

async function deleteAnnouncement(id) {
	await apiRequest('DELETE', '/announcement/api/' + id)
	await updateAnnouncementTable();
	await loadAnnouncements(false);
}

function previewAnnouncement() {
	let oldAnnElm = document.querySelector('.announcement.preview');
	let bar = document.querySelector('nav.navbar-default');

	let ann = getAnnouncement();

	ann.origin = 'preview';

	let newAnnElm = makeAnnouncement(ann);

	newAnnElm.classList.add('preview');

	if (oldAnnElm)
		oldAnnElm.replaceWith(newAnnElm);
	else
		bar.insertAdjacentElement('afterend', newAnnElm);
}

function getAlertColors(className) {
	let elem = document.createElement('div');
	elem.style.display = "none"
	document.body.appendChild(elem)

	elem.classList.add('alert-' + className);

	let style = window.getComputedStyle(elem);

	colors = [style.color, style.backgroundColor];

	elem.parentNode.removeChild(elem)

	return colors;
}

function updateSeverity() {
	let frm = document.getElementById('add-announcement-form');
	let severity = frm.elements.severity.value;

	let colors = getAlertColors(severity);
	
	frm.elements.font_color.value = rgb2hex(colors[0]);
	frm.elements.background_color.value = rgb2hex(colors[1]);

	previewAnnouncement();
}

function getAnnouncement() {
	let frm = document.getElementById('add-announcement-form');

	let ann = {
		message: frm.elements.announcement.value,
		severity: frm.elements.severity.value,
		class: 'alert-' + frm.elements.severity.value,
	}

	if (frm.elements.show_until.value)
		ann.show_until = new Date(frm.elements.show_until.value).toISOString();

	if (frm.elements.show_after.value)
		ann.show_after = new Date(frm.elements.show_after.value).toISOString();

	if (frm.elements.font_color.value)
		ann.font_color = frm.elements.font_color.value;

	if (frm.elements.background_color.value)
		ann.background_color = frm.elements.background_color.value;

	return ann;
}

async function addAnnouncement(ev) {
	let ann = getAnnouncement();

	await apiRequest('POST', '/announcement/api', ann);
	await updateAnnouncementTable();
	await loadAnnouncements();
}

async function loaded() {
	let btnAdd = document.getElementById('add-announcement-button');
	if (btnAdd) {
		btnAdd.addEventListener('click', addAnnouncement);

		updateSeverity();
	}

	let now = new Date();
	let inpShowUntil = document.getElementById('show-until');
	let inpShowAfter = document.getElementById('show-after');
	if (inpShowAfter)
		inpShowAfter.value = now.toInputValue();
	if (inpShowUntil)
		inpShowUntil.value = now.addDays(7).toInputValue();

	await loadAnnouncements();

	let table = document.getElementById('announcements');
	if (table)
		await updateAnnouncementTable();
}

window.addEventListener('load', loaded);