from datetime import datetime
import enum

from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.schema import UniqueConstraint
from sqlalchemy.types import Integer, String, DateTime, JSON, Enum, ARRAY
from sqlalchemy.pool import NullPool
from sqlalchemy import Column

from jupyterhub_services.util import new_token

Base = declarative_base()


class SurveyResult(Base):

    __tablename__ = 'survey_results'

    id = Column(Integer, primary_key=True, autoincrement=True)
    survey_id = Column(Integer)
    username = Column(String)
    completed = Column(DateTime, default=datetime.utcnow)

    def to_dict(self):
        return {
            'id': self.survey_id,
            'completed': self.completed.isoformat()
        }


class Profile(Base):

    __tablename__ = 'profiles'

    slug = Column(String, primary_key=True)
    definition = Column(JSON)
    versions = Column(JSON)
    created = Column(DateTime, default=datetime.utcnow)
    last_update = Column(DateTime, default=datetime.utcnow)
    last_build = Column(JSON)


# class SharePermission(Base):

#     __tablename__ = 'share_permissions'

#     id = Column(Integer, primary_key=True, autoincrement=True)

class Share(Base):

    __tablename__ = 'shares'

    id = Column(Integer, primary_key=True, autoincrement=True)
    created_by = Column(String, nullable=False)
    created = Column(DateTime, default=datetime.utcnow)
    type = Column(String, nullable=False)
    config = Column(JSON, default={})
    mountpoint = Column(String, nullable=False)
    name = Column(String)

    __mapper_args__ = {
        'polymorphic_on': type,
    }

    # permissions = relationship("Share")

    def to_dict(self):
        return {
            'id': self.id,
            'created_by': self.created_by,
            'created': self.created.isoformat(),
            'type': self.type,
            'mountpoint': self.mountpoint,
            'name': self.name,
            'config': self.config
        }


class Coupon(Base):

    __tablename__ = 'coupons'

    id = Column(Integer, primary_key=True, autoincrement=True)
    token = Column(String, default=new_token)
    created = Column(DateTime, default=datetime.utcnow)
    valid_until = Column(DateTime)
    available = Column(Integer)
    groups = Column(String, default='')

    def to_dict(self):
        d = {
            'token': self.token,
            'created': self.created.isoformat(),
            'available': self.available,
            'groups': self.groups
        }

        if self.valid_until is not None:
            d['valid_until'] = self.valid_until.isoformat()

        return d


class Submission(Base):

    __tablename__ = 'submissions'

    id = Column(Integer, primary_key=True, autoincrement=True)
    submitted = Column(DateTime, default=datetime.utcnow)
    submitter = Column(String, nullable=False)
    profile = Column(String, nullable=False)
    realm = Column(String, nullable=False)
    data = Column(JSON, nullable=False)

    def to_dict(self):
        return {
            'id': self.id,
            'submitted': self.submitted.isoformat(),
            'submitter': self.submitter,
            'profile': self.profile,
            'realm': self.realm,
            'data': self.data
        }


class Announcement(Base):

    __tablename__ = 'announcements'

    id = Column(Integer, primary_key=True, autoincrement=True)
    message = Column(String, nullable=False)
    severity = Column(String, default='info')
    font_color = Column(String, nullable=False)
    background_color = Column(String, nullable=False)
    created_at = Column(DateTime, default=datetime.utcnow)
    created_by = Column(String, nullable=False)

    show_after = Column(DateTime, default=datetime.utcnow)
    show_until = Column(DateTime)

    def to_dict(self):
        return {
            'id': self.id,
            'message': self.message,
            'timestamp': self.created_at.isoformat(),
            'user': self.created_by,
            'severity': self.severity,
            'show_until': self.show_until,
            'show_after': self.show_after,
            'font_color': self.font_color,
            'background_color': self.background_color,
            'origin': 'rwth-jupyter'
        }
