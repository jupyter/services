from http import HTTPStatus
import os
import json
from datetime import datetime, timedelta

from tornado.web import HTTPError, MissingArgumentError, authenticated

from jupyterhub.services.auth import HubOAuthenticated
from jupyterhub_services.orm import Coupon
from jupyterhub_services.handlers.base import BaseRequestHandler
from jupyterhub_services.handlers.template import AuthenticatedTemplateHandler


class CouponTemplateHandler(AuthenticatedTemplateHandler):

    def initialize(self, **kwargs):
        super().initialize(template='coupon.html', **kwargs)

    @authenticated
    async def get(self, *args):
        groups = await self.hub_api_request('GET', 'groups', token=self.service.token)

        user = await self.get_user()

        for group in groups:
            group['users_count'] = len(group['users'])

        ns = {
            **self.namespace,
            'groups': groups,
            'user': user,
            'args': args
        }

        self.write_template(self.template, **ns)


class CouponCreationHandler(HubOAuthenticated, BaseRequestHandler):

    hub_users = []
    allow_admin = True

    @authenticated
    def post(self):
        params = json.loads(self.request.body)

        try:
            valid_until = params['valid_until']
            valid_until = datetime.fromisoformat(valid_until)
        except KeyError:
            valid_until = datetime.now() + timedelta(days=31*3)

        groups = params.get('groups', '').split(',')
        groups = [g for g in groups if len(g) > 0]

        avail = int(params.get('available', 1))

        with self.service.session() as session:
            coupon = Coupon(
                available=avail,
                groups=','.join(groups),
                valid_until=valid_until
            )

            session.add(coupon)
            session.flush()

            self.finish({
                **coupon.to_dict(),
                'redeem_url': f'{self.service.host}{self.service.prefix}api/redeem/{coupon.token}'
            })


class CouponRedemptionHandler(HubOAuthenticated, BaseRequestHandler):

    @authenticated
    async def get(self, coupon):
        user = await self.get_user()

        with self.service.session() as session:
            coupon = session.query(Coupon).filter_by(token=coupon).first()

            if coupon is None:
                raise HTTPError(HTTPStatus.NOT_FOUND, reason='Coupon does not exist')

            if coupon.available < 1:
                raise HTTPError(HTTPStatus.NOT_FOUND, reason='Coupon is not available anymore')

            groups = coupon.groups.split(',')
            groups = [g for g in groups if len(g) > 0]

            missing_groups = set(groups) - set(user['groups'])

            for group in missing_groups:
                await self.hub_add_user_to_group(group, user['name'])

            self.hub_auth.clear_cookie(self)

            coupon.available = coupon.available - 1

            self.finish(coupon.to_dict())

