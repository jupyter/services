import json

from http import HTTPStatus
from tornado.web import HTTPError, authenticated

from jupyterhub.services.auth import HubOAuthenticated
from jupyterhub_services.orm import Submission
from jupyterhub_services.handlers.base import BaseRequestHandler


class SubmissionHandler(HubOAuthenticated, BaseRequestHandler):

    MAX_BODY_SIZE = 4 << 20 # 4 MiB

    @authenticated
    async def delete(self, profile, realm, submitter=None, ident=None):
        user = await self.get_user()

        flt = {
            'profile': profile,
            'realm': realm
        }

        if ident:
            flt['id'] = ident

        if submitter:
            if submitter == 'me':
                submitter = user['name']

            flt['submitter'] = submitter
        else:
            if (not user['admin'] and
                f'submission-manager@{realm}' not in user['groups'] and
                f'profile-manager@{profile}' not in user['groups']):
                raise HTTPError(HTTPStatus.UNAUTHORIZED, reason='Unauthorized')

        with self.service.session() as session:
            session.query(Submission).filter_by(**flt).delete()

    @authenticated
    async def get(self, profile, realm, submitter=None, ident=None):
        user = await self.get_user()

        flt = {
            'profile': profile,
            'realm': realm
        }

        if ident:
            flt['id'] = ident

        if submitter:
            if submitter == 'me':
                submitter = user['name']

            flt['submitter'] = submitter
        else:
            if (not user['admin'] and
                f'submission-manager@{realm}' not in user['groups'] and
                f'profile-manager@{profile}' not in user['groups']):
                raise HTTPError(HTTPStatus.UNAUTHORIZED, reason='Unauthorized')

        with self.service.session() as session:
            submissions = session.query(Submission).filter_by(**flt).order_by(
                Submission.submitted.desc()
            )

            limit = self.get_argument('limit', None)
            if limit is not None:
                submissions = submissions.limit(int(limit))

            self.finish({
                'realm': realm,
                'submissions': [s.to_dict() for s in submissions]
            })

    @authenticated
    async def post(self, profile, realm):
        user = await self.get_user()

        if len(self.request.body) > self.MAX_BODY_SIZE:
            raise HTTPError(HTTPStatus.REQUEST_ENTITY_TOO_LARGE, reason='Payload too large')

        data = json.loads(self.request.body)

        with self.service.session() as session:
            submission = Submission(
                profile=profile,
                realm=realm,
                submitter=user['name'],
                data=data
            )

            session.add(submission)

            self.finish({
                'profile': profile,
                'realm': realm,
                'submissions': [submission.to_dict()]
            })
