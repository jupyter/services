from bs4 import BeautifulSoup
import base64
import json
from datetime import datetime, timedelta
from http import HTTPStatus
from urllib.parse import urlencode

from tornado.web import HTTPError, authenticated
from jupyterhub.services.auth import HubOAuthCallbackHandler
from jupyterhub_services.handlers.base import BaseRequestHandler
from jupyterhub_services.util import async_requests
from jupyterhub_services.util import async_requests


class ScieboEndpointHandler(BaseRequestHandler):

    ENDPOINT_URL = 'https://www.sciebo.de/de/login/index.html'

    async def _get_endpoints(self):
        k = 'sciebo-endpoints'
        try:
            return await self.service.cache.get(k)
        except KeyError:
            pass

        response = await async_requests.get(self.ENDPOINT_URL)

        bs = BeautifulSoup(response.content, 'html.parser')

        pbox = bs.find_all(id='t1_Pbox')
        tbody = pbox[0].find_all('tbody')
        rows = tbody[0].find_all('tr')

        ep = []
        for row in rows:
            name = row.find_all('td')[0].contents[0]
            url = row.find_all('td')[3].find_all('a')[0].contents[0]

            ep.append({
                'name': str(name),
                'url': str(url)
            })

        await self.service.cache.set(k, ep, 24*60*60)

        return ep

    async def get(self):
        ep = await self._get_endpoints()

        self.write({
            'endpoints': ep
        })


class ScieboOAuthCallbackHandler(HubOAuthCallbackHandler, BaseRequestHandler):

    @authenticated
    async def get(self):
        code = self.get_query_argument('code')

        if not code:
            raise HTTPError(HTTPStatus.BAD_REQUEST)

        r = await async_requests.post('https://rwth-aachen.sciebo.de/index.php/apps/oauth2/api/v1/token',
                                      data={
                                         'grant_type': 'authorization_code',
                                         'code': code,
                                         'redirect_uri': 'https://jupyter.rwth-aachen.de/services/share/api/oauth_callback'
                                      },
                                      auth=(
                                          self.service.sciebo_client_id,
                                          self.service.sciebo_client_secret
                                      ))
        r.raise_for_status()

        token = r.json()

        state_encoded = self.get_query_argument('state')
        state = json.loads(base64.b64decode(state_encoded))

        config = state.setdefault('config', {})
        config['user_id'] = token['user_id']

        state_encoded = base64.b64encode(json.dumps(state).encode('utf-8'))

        # Store Sciebo OAuth2 access token as cookie
        expires = datetime.now() + timedelta(seconds=token['expires_in'])
        self.set_cookie('sciebo_access_token', token['access_token'], expires=expires)
        self.set_cookie('sciebo_server', config['server'], expires=expires)

        self.redirect(self.service.prefix + '?' + urlencode({
            'state': state_encoded
        }))

        # {
        #     "access_token": "z2KfPKcOe4Y9vTHxtf63ydkXsE3fHUsOQL6jjvXVwOYwduWYfdExpIDOHHKlYXoW",
        #     "token_type": "Bearer",
        #     "expires_in": 3600,
        #     "refresh_token": "rM9d7MPGxm691POoAQ1yQKMBjVzhAXxw0uc2DRON0w8YUssTQcUpK3PPV1qUdcxh",
        #     "user_id": "3DPT00@rwth-aachen.de",
        #     "message_url": "https://rwth-aachen.sciebo.de/apps/oauth2/authorization-successful"
        # }
