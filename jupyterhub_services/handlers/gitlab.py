import json
import functools
import re
import magic

from smtplib import SMTP
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from http import HTTPStatus
from tornado.web import authenticated, HTTPError

from jupyterhub.services.auth import HubOAuthenticated
from jupyterhub_services.handlers.base import BaseRequestHandler


class GitLabBuildHandler(HubOAuthenticated, BaseRequestHandler):

    @authenticated
    async def post(self, slug):
        """ Request a new build """

        user = await self.get_user(full=True)

        if (f'profile-manager@{slug}' not in user['groups'] and
           not user['admin']):
            raise HTTPError(HTTPStatus.UNAUTHORIZED, reason=f'User is not a manager of the profile {slug}')

        vars = {
            'PROFILES': slug,
            'TRIGGER_USERNAME': user['name'],
            'TRIGGER_MAIL': user['mail']
        }

        pl = self.service.project.pipelines.create({
            'ref': 'master',
            'variables': [{
                'key': key,
                'value': value,
                'variable_type': 'env_var'
            } for key, value in vars.items()]
        })

        self.finish(pl._attrs)


class GitLabArtifactHandler(BaseRequestHandler):

    async def get(self, job, path):
        art = await self.service.get_artifact(job, path)

        self.set_header('Content-Type', magic.from_buffer(art, mime=True))
        self.finish(art)


class GitLabVersionHandler(GitLabArtifactHandler):

    VERSION_REPOS = ['pip', 'conda', 'jupyter-serverextensions', 'jupyter-labextensions', 'jupyter-kernelspecs']

    def get(self, slug, repo):
        if repo not in self.VERSION_REPOS:
            raise HTTPError(HTTPStatus.BAD_REQUEST, reason='Invalid repo type')

        super().get(slug, f'versions/{repo}.json')


def authenticated_webhook(method):
    @functools.wraps(method)
    def wrapper(self, *args, **kwargs):
        if self.request.headers.get('x-gitlab-token', '') != self.service.gitlab_webhook_token:
            raise HTTPError(HTTPStatus.FORBIDDEN, "Invalid or missing token")

        return method(self, *args, **kwargs)

    return wrapper


class GitLabHookHandler(BaseRequestHandler):

    def escape_ansi(self, line):
        ansi_escape = re.compile(r'(\x9B|\x1B\[)[0-?]*[ -\/]*[@-~]')

        return ansi_escape.sub('', line)

    def send_mail(self, profile, build, trigger_username, trigger_mail):
        sender = 'jupyter-admin@lists.rwth-aachen.de'

        slug = profile['slug']

        # receipients = {
        #     u['mail']: u['name'] for u in profile['contacts']
        # }
        receipients = {}

        # receipients.update({
        #     'jupyter-admin@lists.rwth-aachen.de': 'Jupyter Admins'
        # })

        if trigger_mail and trigger_username:
            receipients[trigger_mail] = trigger_username

        msg = MIMEMultipart()

        if build.status in ['created', 'skipped', 'canceled']:
            verb = 'has been ' + build.status
        elif build.status == 'success':
            verb = 'was successful'
        elif build.status in ['pending', 'running']:
            verb = 'is ' + build.status
        elif build.status in ['failed']:
            verb = 'has ' + build.status
        else:
            verb = build.status

        msg['Subject'] = f'[RWTHjupyter] Build of profile {slug} {verb}!'
        msg['From'] = f'Jupyter Admins <{sender}>'

        if build.status != 'success':
            msg['X-Priority'] = '1'
            msg['X-MSMail-Priority'] = 'High'
            msg['Importance'] = 'High'

        if self.service.debug or True:
            receipients.update({
                'svogel2@eonerc.rwth-aachen.de': 'Steffen Vogel'
            })

        msg['To'] = ','.join(f'{name} <{mail}' for mail, name in receipients.items())

        body_text = self.render_template('mails/build.txt',
                                         sender=sender,
                                         profile=profile,
                                         build=build.attributes,
                                         trigger_mail=trigger_mail,
                                         trigger_username=trigger_username,
                                         verb=verb,
                                         public_url=self.service.public_url)

        body = MIMEText(body_text)
        msg.attach(body)

        build_log_text = build.trace().decode('utf-8')
        if build_log_text:
            build_log_text = self.escape_ansi(build_log_text)

            build_log = MIMEText(build_log_text)
            build_log.add_header('Content-Disposition', 'attachment', filename='build_log.txt')

            msg.attach(build_log)

        with SMTP('smarthost.rwth-aachen.de', port=25) as server:
            server.send_message(msg, sender, receipients.keys())

    @authenticated_webhook
    async def post(self):
        req = json.loads(self.request.body)

        self.log.info('Received Webhook: %s', req)

        if req['object_kind'] == 'build' and req['ref'] == 'master':
            # Fetch build job from GitLab API
            build = self.service.project.jobs.get(req['build_id'])

            pipeline = self.service.project.pipelines.get(build.pipeline['id'])
            pipeline_vars = {v.key: v.value for v in pipeline.variables.list()}
            pipeline_profiles = pipeline_vars.get('PROFILES').split(',')
            pipeline_trigger_username = pipeline_vars.get('TRIGGER_USERNAME')
            pipeline_trigger_mail = pipeline_vars.get('TRIGGER_MAIL')

            if pipeline_trigger_username == '${TRIGGER_USERNAME}':
                pipeline_trigger_username = None
            if pipeline_trigger_mail == '${TRIGGER_MAIL}':
                pipeline_trigger_mail = None

            self.log.info('Profiles: %s', pipeline_profiles)

            if pipeline_trigger_username and pipeline_trigger_mail:
                self.log.info('Trigger user: %s <%s>', pipeline_trigger_username, pipeline_trigger_mail)

            if not build:
                raise HTTPError(HTTPStatus.BAD_REQUEST, reason='Missing build details')

            if build.stage == 'build' and build.name == 'generate':
                # Prewarm and update cache
                await self.service.get_profiles(cached=False, ttl=365*24*60*60)

            elif build.stage not in ['prepare', 'check', 'build', 'test']:
                # Prewarm and update cache
                await self.service.cache.set(f'last-{build.status}-build-{build.name}', build.attributes, ttl=365*24*60*60)
                await self.service.cache.set(f'last-build-{build.name}',                build.attributes, ttl=365*24*60*60)

                if build.status == 'success':
                    # Prewarm and update cache
                    await self.service.get_versions(build.name, cached=False, ttl=365*24*60*60)

                if build.status == 'failed' or (build.status != 'created' and build.name in pipeline_profiles):
                    profile = await self.service.get_profile(build.name)

                    self.send_mail(profile, build, pipeline_trigger_username, pipeline_trigger_mail)

        self.finish()
