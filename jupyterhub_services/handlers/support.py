from jupyterhub_services.handlers.base import BaseRequestHandler

from tornado.web import authenticated

from jupyterhub.services.auth import HubOAuthenticated
import datetime

class SupportDownloadHandler(HubOAuthenticated, BaseRequestHandler):

    @authenticated
    async def get(self, type):
        user = await self.get_user(full=True, progress=True)

        if type == 'download':
            now = datetime.datetime.utcnow()
            fn = ('rwth-jupyter-support-info-' +
                user['name'] + '-' +
                now.strftime('%Y-%m-%d_%H-%M-%S.json'))

            self.set_header('Content-Disposition', 'attachment; filename=' + fn)

        self.write(user)
