from http import HTTPStatus
import humanize
import datetime
from dateutil import tz
from tornado.web import HTTPError, authenticated

from jupyterhub.services.auth import HubOAuthenticated
from jupyterhub_services.handlers.base import BaseRequestHandler
from jupyterhub_services.handlers.template import AuthenticatedTemplateHandler


class WhoAmIHandler(AuthenticatedTemplateHandler):

    def initialize(self, **kwargs):
        super().initialize('whoami.html', **kwargs)

    @authenticated
    async def get(self, name):
        current_user = self.get_current_user()

        if name == '' or name == 'me':
            name = current_user['name']
        else:
            if not current_user['admin']:
                raise HTTPError(HTTPStatus.UNAUTHORIZED, reason='You need to admin privileges to get details of other users')

        user = await self.get_user(name=name,
                                   full=True,
                                   progress=True,
                                   quota=False,
                                   moodle=True)

        await self.service.get_moodle_course_details(self, user['moodle']['courses'])

        tzone = tz.tzlocal()
        now = datetime.datetime.now(tzone)

        user['created'] = user['created'].astimezone(tzone)
        user['last_activity'] = user['last_activity'].astimezone(tzone)

        user['created_human'] = humanize.naturaltime(now - user['created'])
        user['last_activity_human'] = humanize.naturaltime(now - user['last_activity'])

        ns = {
            **self.namespace,
            'user': user.copy(),
        }

        self.write_template(self.template, **ns)


class WhoAmIAPIHandler(HubOAuthenticated, BaseRequestHandler):

    @authenticated
    async def get(self, name):
        current_user = self.get_current_user()

        if name == '' or name == 'me':
            name = current_user['name']
        else:
            if not current_user['admin']:
                raise HTTPError(HTTPStatus.UNAUTHORIZED, reason='You need to admin privileges to get details of other users')

        full = self.get_query_argument('full', False)
        progress = self.get_query_argument('progress', False)
        quota = self.get_query_argument('quota', False)
        moodle = self.get_query_argument('moodle', False)

        user = await self.get_user(name=name,
                                   full=full,
                                   progress=progress,
                                   quota=quota,
                                   moodle=moodle)

        if moodle:
            await self.service.get_moodle_course_details(self, user['moodle']['courses'])

        self.finish(user)
