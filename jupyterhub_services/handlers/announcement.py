from datetime import datetime, timedelta
from http import HTTPStatus
from tornado import escape
from tornado.web import authenticated, HTTPError
from lxml import etree
from email.utils import parsedate_to_datetime
from dateutil import tz

from jupyterhub.services.auth import HubOAuthenticated
from jupyterhub_services.handlers.base import BaseRequestHandler
from jupyterhub_services.orm import Announcement
from jupyterhub_services.util import async_requests, remove_prefix


class AnnouncementHandler(HubOAuthenticated, BaseRequestHandler):
    """Dynamically manage page announcements"""

    hub_users = []
    allow_admin = True

    @authenticated
    async def post(self, id):
        """Add announcement"""
        user = await self.get_user()
        doc = escape.json_decode(self.request.body)

        with self.service.session() as session:

            # Create new
            if id is None:
                if (not doc.get('message') or
                    not doc.get('font_color') or
                    not doc.get('background_color') or
                    not doc.get('severity')):
                    raise HTTPError(HTTPStatus.BAD_REQUEST, reason='Malformed request')

                announcement = Announcement(
                    message=doc.get('message'),
                    severity=doc.get('severity'),
                    font_color=doc.get('font_color'),
                    background_color=doc.get('background_color'),
                    created_by=user['name'],
                    show_after=doc.get('show_after', datetime.utcnow()),
                    show_until=doc.get('show_until', datetime.utcnow() +
                                       timedelta(days=365))
                )

                session.add(announcement)

            # Update existing
            else:
                announcement = session.query(Announcement).filter_by(id=id)
                if announcement is None:
                    raise HTTPError(HTTPStatus.NOT_FOUND, reason='Announcement not found')

                if 'announcement' in doc:
                    announcement.message = doc['message']

                if 'severity' in doc:
                    announcement.severity = doc['severity']

                if 'font_color' in doc:
                    announcement.font_color = doc['font_color']

                if 'background_color' in doc:
                    announcement.font_color = doc['background_color']

                if 'show_after' in doc:
                    announcement.show_after = datetime.fromisoformat(
                        doc['show_after'])

                if 'show_until' in doc:
                    announcement.show_until = datetime.fromisoformat(
                        doc['show_until'])

            session.flush()

            self.finish({
                'announcement': announcement.to_dict()
            })

    @authenticated
    def delete(self, id):
        """Delete announcement"""

        with self.service.session() as session:
            session.query(Announcement).filter_by(id=id).delete()

        self.finish({})

    async def get(self, id):
        """Retrieve announcement"""

        all = self.get_query_argument('all', 'false')
        all = all == 'true'

        itc_feed = await self.get_itc_feed_announcements(all)
        gitlab = await self.get_rwth_gitlab_announcements(all)

        with self.service.session() as session:
            if all:
                announcements = session.query(Announcement)
            else:
                current_time = datetime.now()
                announcements = session.query(Announcement).filter(
                    Announcement.show_until > current_time,
                    Announcement.show_after < current_time
                )

            self.finish({
                'announcements': [a.to_dict() for a in announcements] + itc_feed + gitlab
            })

    async def get_rwth_gitlab_announcements(self, all=False):
        gitlab_api = 'https://git.rwth-aachen.de/api/v4/broadcast_messages'

        r = await async_requests.get(gitlab_api)
        r.raise_for_status()

        announcements = []

        now = datetime.now(tz.UTC)
        for ann in r.json():
            ann['show_until'] = datetime.fromisoformat(ann['ends_at'])
            ann['show_after'] = datetime.fromisoformat(ann['starts_at'])

            if not all and (
                    ann['show_until'] > now or
                    ann['show_after'] < now or
                    not ann['active'] or
                    not ann['message'] or
                    ann['broadcast_type'] != 'banner' or
                    ann['target_path'] != ''):
                continue

            ann['background_color'] = ann['color']
            ann['font_color'] = ann['font']
            ann['origin'] = 'rwth-gitlab'
            ann['id'] = -1000 - ann['id']

            del ann['font']
            del ann['color']
            del ann['ends_at']
            del ann['starts_at']

            langs = ann['message'].split(' / ')
            ann['message'] = langs[1] if len(langs) >= 2 else ann['message']

            announcements.append(ann)

        return announcements

    async def get_itc_feed_announcements(self, all):
        queue = '68-rwthjupyter'
        feed = f'https://maintenance.rz.rwth-aachen.de/ticket/status/messages/{queue}/feed.rss'

        r = await async_requests.get(feed)
        r.raise_for_status()
        root = etree.XML(r.content)

        announcements = []

        id = -100
        for item in root.iterfind(".//item"):
            title = item.find('title').text
            link = item.find('link').text
            description = item.find('description').text
            pubDate = item.find('pubDate').text

            title = remove_prefix(title, 'RWTHjupyter - ')

            dt = parsedate_to_datetime(pubDate)
            now = datetime.now(tz.UTC)
            if not all and dt > now:
                continue

            announcements.append({
                'message': f'<a title="{description}" href="{link}">{title}</a>',
                'timestamp': dt.isoformat(),
                'id': id,
                'font_color': '#31708f', # TODO the ITC feed does not provide any severity
                'background_color': '#d9edf7',
                'origin': 'rwth-itc-feed'
            })

            id -= 1

        return announcements
