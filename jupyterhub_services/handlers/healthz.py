from tornado.web import RequestHandler

class HealthzCallbackHandler(RequestHandler):

    def get(self):
        self.finish("OK")
