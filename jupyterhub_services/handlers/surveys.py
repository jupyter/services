from tornado.web import authenticated

from jupyterhub.services.auth import HubOAuthenticated
from jupyterhub_services.handlers.base import BaseRequestHandler
from jupyterhub_services.orm import SurveyResult


class SurveyCompletionHandler(HubOAuthenticated, BaseRequestHandler):

    @authenticated
    async def get(self, id):
        user = await self.get_user()

        with self.service.session() as session:
            result = SurveyResult(
                survey_id=id,
                username=user['name']
            )

            session.add(result)

        self.redirect('/')


class SurveyHandler(HubOAuthenticated, BaseRequestHandler):

    @authenticated
    async def get(self):
        user = await self.get_user()

        with self.service.session() as session:
            results = session.query(SurveyResult).filter_by(
                username=user['name']
            )

            self.finish({
                'surveys': [r.to_dict() for r in results]
            })
