from http import HTTPStatus
import re
import json
import requests
import dateutil.parser
import traceback

from tornado.web import RequestHandler, HTTPError

from jupyterhub.utils import url_path_join

from jupyterhub.services.auth import HubOAuthenticated
from jupyterhub_services.services import *
from jupyterhub_services.util import api_request
from jupyterhub_services.ceph import CephfsPVC
from jupyterhub_services.util import async_requests


class BaseRequestHandler(RequestHandler):

    def initialize(self, service):
        self.service = service
        self.log = service.log

    def write_error(self, status_code, exc_info=None, **kwargs):

        additional = {}

        if exc_info:
            exc = exc_info[1]

            if isinstance(exc, HTTPError):
                additional['message'] = exc.log_message
            else:
                additional['message'] = str(exc)

            if self.settings.get('serve_traceback'):
                # in debug mode, try to send a traceback
                lines = []

                for line in traceback.format_exception(*exc_info):
                    lines.append(line.strip())

                additional['traceback'] = lines

        self.finish({
            'error': {
                'code': status_code,
                'reason': self._reason,
                **additional
            }
        })

    async def get_user(self, name=None, full=False, progress=False,
                       quota=False, moodle=False):
        if name is None:
            user = self.get_current_user()
            name = user['name']
        else:
            full = True

        if full:
            user = await self.hub_api_request('GET', f'users/{name}',
                                              token=self.service.token)

            user['headers'] = dict(self.request.headers)
            user['remote-ip'] = self.request.remote_ip
            user['mail'] = user['auth_state']['Mail']
            user['ids'] = {}

            exp = re.compile(r'urn(?::[a-z0-9.-]+)*:'
                             'name=([a-z0-9._-]+):value=([a-z0-9._-]+)')
            ids = user['auth_state'].get('Rwthsystemids')
            if ids:
                for id in ids.split(','):
                    m = exp.match(id)
                    if m:
                        name = m.group(1)
                        value = m.group(2)

                        user['ids'][name] = value

            affiliations = user['auth_state'].get('Edupersonscopedaffiliation')
            if affiliations is None:
                user['affiliations'] = []
            else:
                user['affiliations'] = affiliations.split(';')

            if progress:
                for server in user['servers'].values():
                    server['progress'] = await self.get_progress(
                        user['name'], server['name'])

        if quota:
            vol = CephfsPVC('claim-' + user['name'], 'jhub-users',
                            self.service)
            user['quota'] = await vol.get_quota()

        if moodle:
            course_map = {}
            for group in user['groups']:
                m = re.match('moodle-([a-z0-9]+)@(\d+)', group)
                if m:
                    course_id = int(m.group(2))
                    role = m.group(1)

                    try:
                        course = course_map[course_id]
                    except KeyError:
                        course = {
                            'roles': [],
                            'course_id': course_id,
                        }

                        course_map[course_id] = course

                    course['roles'].append(role)

            user['moodle'] = {
                'courses': course_map.values()
            }

        if isinstance(user['created'], str):
            user['created'] = dateutil.parser.parse(user['created'])
        if isinstance(user['last_activity'], str):
            user['last_activity'] = dateutil.parser.parse(
                user['last_activity'])

        return user

    async def get_progress(self, user, server=None):
        if server:
            url = f'/users/servers/{server}/progress'
        else:
            url = f'/users/{user}/server/progress'

        updates = []

        try:
            resp = await self.hub_api_request('GET', url,
                                              token=self.service.token,
                                              headers={
                                                  'Accept': 'text/event-stream'
                                              },
                                              stream=True,
                                              timeout=0.2)

            for raw_line in resp.iter_lines():
                if raw_line:
                    line = raw_line.decode('utf-8')

                    if line.startswith('data: '):
                        payload_raw = line[6:]
                        payload = json.loads(payload_raw)

                        updates.append(payload)

        except requests.exceptions.ConnectionError:
            pass

        return updates

    async def hub_api_request(self, method, endpoint, **kwargs):
        """Make an API request"""

        token = kwargs.pop('token', self.hub_auth.api_token)

        try:
            return await api_request(method, endpoint,
                                     token=token,
                                     api_url=self.hub_auth.api_url,
                                     **kwargs)

        except requests.exceptions.Timeout:
            raise HTTPError(HTTPStatus.REQUEST_TIMEOUT,
                            reason='Hub API request timed out')
        except requests.RequestException as e:
            api_resp = e.response.json()
            api_msg = api_resp['message']

            msg = f'API request failed: {api_msg}'

            raise HTTPError(HTTPStatus.INTERNAL_SERVER_ERROR, reason=msg)

    async def hub_ensure_group(self, name):
        try:
            group = await self.hub_api_request('GET', f'groups/{name}',
                                               token=self.service.token)
        except HTTPError:
            group = await self.hub_api_request('POST', f'groups/{name}',
                                               token=self.service.token)

        return group

    async def hub_add_user_to_group(self, group, user):
        group = await self.hub_ensure_group(group)
        group_name = group.get('name')

        if user in group['users']:
            return

        r = await self.hub_api_request('POST', f'groups/{group_name}/users',
                                       token=self.service.token,
                                       data=json.dumps({
                                           'users': [user]
                                       }))

        return r

    async def moodle_api_request(self, function, params, **kwargs):
        url = url_path_join(self.service.moodle_url,
                            'webservice', 'rest', 'server.php')

        params.update({
            'wstoken': self.service.moodle_token,
            'wsfunction': function,
            'moodlewsrestformat': 'json'
        })

        r = await async_requests.get(url, params=params, **kwargs)
        r.raise_for_status()

        j = r.json()

        if 'exception' in j:
            raise RuntimeError('Moodle ' + j['exception'] +
                               ' (' + j['errorcode'] + '): ' + j['message'])

        return j

    # Template Rending

    def get_template(self, name):
        """Return the jinja template object for a given name"""

        return self.service.jinja_env.get_template(name)

    def render_template(self, name, **ns):
        template_ns = {}
        template_ns.update(self.template_namespace)
        template_ns.update(ns)
        template = self.get_template(name)

        return template.render(**template_ns)

    def write_template(self, name, **ns):
        out = self.render_template(name, **ns)

        self.finish(out)

    @property
    def template_namespace(self):
        ns = {}

        if 'template_vars' in self.settings:
            ns.update(self.settings['template_vars'])

        return ns
