import re

from sqlalchemy import or_

from tornado import escape
from tornado.web import HTTPError, authenticated
from http import HTTPStatus

from jupyterhub.services.auth import HubOAuthCallbackHandler
from jupyterhub_services.handlers.base import BaseRequestHandler
from jupyterhub_services.share import ScieboShare, Share


class ShareHandler(HubOAuthCallbackHandler, BaseRequestHandler):

    allow_all = True
    ROLE_REGEX = re.compile(r'share-(manager|reader|writer)-([0-9]+)')

    @authenticated
    async def post(self, id):
        """Add share"""
        user = await self.get_user()
        doc = escape.json_decode(self.request.body)

        # TODO: check if user is allowed to create shares

        type = doc.get('type', 'pvc')
        name = doc.get('name')
        config = doc.get('config', {})
        mountpoint = doc.get('mountpoint')

        # Input validation
        if id is not None:
            raise HTTPError(HTTPStatus.BAD_REQUEST, reason='Updating an '
                            'existing share not supported yet')

        if type not in ['pvc', 'webdav', 'sciebo']:
            raise HTTPError(HTTPStatus.BAD_REQUEST, reason='Unsupported type')

        if not name:
            raise HTTPError(HTTPStatus.BAD_REQUEST, reason='Missing name')

        if len(name) > 64:
            raise HTTPError(HTTPStatus.BAD_REQUEST, reason='Name is too long')

        if not mountpoint:
            raise HTTPError(HTTPStatus.BAD_REQUEST, reason='Missing mountpoint')

        if not re.match(r'[A-Za-z0-9-_]+', mountpoint):
            raise HTTPError(HTTPStatus.BAD_REQUEST, reason='Invalid mountpoint')

        with self.service.session() as session:

            share = Share.class_from_type(type)(name, mountpoint, user['name'],
                                                **config)

            if isinstance(share, ScieboShare):
                token = self.get_cookie('sciebo_access_token')
                await share.create(token)

            if not share.validate():
                raise HTTPError(HTTPStatus.BAD_REQUEST, reason='Failed to validate share config')

            session.add(share)
            session.flush()  # get ID of share

            await share.create_k8s()

            # Create new share group
            for group in [f'share-{role}@{share.id}' for role in ['manager',
                                                                  'reader',
                                                                  'writer']]:
                await self.hub_ensure_group(group)
                await self.hub_add_user_to_group(group, user['name'])

            # Update share object with new groups
            # share.group_readers.append(f'share-reader@{share.id}')
            # share.group_writers.append(f'share-writer@{share.id}')
            # share.group_manager.append(f'share-manager@{share.id}')

            self.log.info('Added share: %s', share)

        self.finish({})

    @authenticated
    async def delete(self, id):
        """Delete share"""
        # user = await self.get_user()

        with self.service.session() as session:
            share = session.query(Share).filter_by(id=id).one()

            if isinstance(share, ScieboShare):
                await share.delete()

            # TODO: Check if used allowed to access share

            share.delete_k8s()

            session.delete(share)

        self.finish({})

    @authenticated
    async def get(self, id):
        """Retrieve shares"""

        user = await self.get_user()

        shares = self._get_shares(user)
        share_ids = shares.keys()

        result = []

        with self.service.session() as session:
            shares_orm = session.query(Share).filter(
                or_(
                    Share.created_by == user['name'],
                    Share.id.in_(share_ids)
                )
            )

            for share in shares_orm:
                roles = shares.get(share.id, [])

                result.append({
                    'role': shares.get(share.id, []),
                    'pvc': f'jupyter-share-{share.id}',
                    'read_only': 'manager' not in roles
                                 and 'user-rw' not in roles,
                    **share.to_dict()
                })

            self.finish({
                'shares': result
            })

    def _get_shares(self, user):
        shares = {}

        for group in user['groups']:
            m = self.ROLE_REGEX.match(group)
            if m:
                role = m.group(1)
                id = int(m.group(2))

                if id not in shares:
                    shares[id] = []

                shares[id].append(role)

        return shares
