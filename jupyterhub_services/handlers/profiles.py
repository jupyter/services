from http import HTTPStatus
import time
import json
import spdx_license_list

from tornado.web import HTTPError

from jupyterhub_services.handlers.base import BaseRequestHandler
from jupyterhub_services.handlers.template import TemplateHandler
from jupyterhub_services.handlers.gitlab import GitLabArtifactHandler


class ProfilesHandler(BaseRequestHandler):

    async def get(self, slug=None):
        pl = await self.service.get_profiles()

        if slug and slug != 'all':
            version = self.get_query_argument('version', False)

            p = [p for p in pl if p['slug'] == slug][0]

            p['last_build'] = await self.service.get_last_build(p['slug'])

            if version:
                p['versions'] = await self.service.get_versions(p['slug'])

            self.finish({
                'profile': self._filter_profile(p)
            })
        else:
            version = self.get_query_argument('version', False)

            for p in pl:
                p['last_build'] = await self.service.get_last_build(p['slug'])

                if version:
                    p['versions'] = await self.service.get_versions(p['slug'])

            self.finish({
                'profiles': [self._filter_profile(p) for p in pl]
            })

    def _filter_profile(self, p):
        """ Filter out certain credentials """

        if 'kubespawner' in p:
            del p['kubespawner']

        if 'notebooks' in p:
            if 'repo' in p['notebooks']:
                if 'password' in p['notebooks']['repo']:
                    del p['notebooks']['repo']['password']

        if 'image' in p:
            if 'repo' in p['image']:
                if 'password' in p['image']['repo']:
                    del p['image']['repo']['password']

        return p


class ProfileGraphHandler(GitLabArtifactHandler):

    def get(self):
        super().get('generate', 'graph.svg')


class ProfileListHandler(TemplateHandler):

    def initialize(self, **kwargs):
        super().initialize('profile-list.html', **kwargs)

    async def get(self, page):
        build = await self.service.get_last_build('generate', status='success')
        if not build:
            raise HTTPError(HTTPStatus.INTERNAL_SERVER_ERROR, reason='Failed to find last successful build job')

        job = self.service.project.jobs.get(build['id'])
        art = job.artifact('profile-definitions.json')
        if not art:
            raise HTTPError(HTTPStatus.INTERNAL_SERVER_ERROR, reason='Failed to find profile definitions')

        profiles = json.loads(art)

        config = {
            'JUPYTERHUB_URL': self.service.host,
            'PROJECT_URL': 'https://git.rwth-aachen.de/jupyter/profiles',
            'DOCKER_REGISTRY': 'registry.git.rwth-aachen.de/jupyter/profiles',
        }

        ns = {
            **self.namespace,
            'profiles': profiles,
            'config': config,
            'job': job,
            'args': [page]
        }

        self.write_template(self.template, **ns)


class ProfileFormHandler(TemplateHandler):

    def initialize(self, **kwargs):
        super().initialize('profile-form.html', **kwargs)


class SPDXLicenseHandler(BaseRequestHandler):

    async def get(self):
        self.write({
            'licenses': dict(spdx_license_list.LICENSES)
        })


class ProfileDetailHandler(TemplateHandler):

    def initialize(self, **kwargs):
        super().initialize('profile-details.html', **kwargs)

    @property
    def namespace(self):
        config = {
            'JUPYTERHUB_URL': self.service.host,
            'PROJECT_URL': 'https://git.rwth-aachen.de/jupyter/profiles',
            'DOCKER_REGISTRY': 'registry.git.rwth-aachen.de/jupyter/profiles',
        }

        return {
            'config': config,
            **super().namespace
        }

    async def get(self, page, slug):
        build_generate = await self.service.get_last_build('generate', status='success')
        if not build_generate:
            raise HTTPError(HTTPStatus.INTERNAL_SERVER_ERROR, reason='Failed to find last successful build job')

        job_generate = self.service.project.jobs.get(build_generate['id'])

        last_build = await self.service.get_last_build(slug)
        if not last_build:
            raise HTTPError(HTTPStatus.INTERNAL_SERVER_ERROR, reason='Failed to find last successful build job')

        last_job = self.service.project.jobs.get(last_build['id'])

        last_pipeline = self.service.project.pipelines.get(last_job.pipeline['id'])
        last_pipeline_vars = {v.key: v.value for v in last_pipeline.variables.list()}

        ns = {
            'profile': await self.service.get_profile(slug),
            'versions': await self.service.get_versions(slug),
            'job': job_generate,
            'last_job': last_job,
            'last_pipeline': last_pipeline,
            'last_pipeline_vars': last_pipeline_vars,
            'args': [page, slug],
            **self.namespace
        }

        self.write_template(self.template, **ns)


class ProfileDetailsHandler(TemplateHandler):

    def initialize(self, **kwargs):
        super().initialize('profile-details.html', **kwargs)

    async def get(self, slug):
        ns = {
            'profile': await self.service.get_profile(slug),
            **self.namespace
        }

        self.write_template(self.template, **ns)
