from tornado.web import authenticated

from jupyterhub.services.auth import HubOAuthenticated
from jupyterhub_services.handlers.base import BaseRequestHandler


class TemplateHandler(BaseRequestHandler):

    def initialize(self, template, **kwargs):
        super().initialize(**kwargs)

        self._namespace = kwargs.get('namespace', {})

        self.template = template

    @property
    def namespace(self):
        from jupyterhub_services.services import services

        return {
            'services': services,
            'service': self.service,
            **self._namespace
        }


    def get(self, *args):
        ns = {
            **self.namespace,
            'args': list(args)
        }

        print(list(args))

        self.write_template(self.template, **ns)


class AuthenticatedTemplateHandler(HubOAuthenticated, TemplateHandler):
    # hub_users can be a set of users who are allowed to access the service
    # `getuser()` here would mean only the user who started the service
    # can access the service:

    # hub_users = {getuser()}

    def initialize(self, template, user_full=False, user_progress=False, **kwargs):
        super().initialize(template, **kwargs)

        self.template = template
        self.user_full = user_full
        self.user_progress = user_progress

    @authenticated
    async def get(self, *args):
        user = await self.get_user(full=self.user_full,
                                   progress=self.user_progress)

        ns = {
            **self.namespace,
            'user': user.copy(),
            'args': list(args)
        }

        self.write_template(self.template, **ns)
