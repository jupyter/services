import re
import pickle
from tornado.web import HTTPError, authenticated
from http import HTTPStatus

from jupyterhub.services.auth import HubOAuthenticated
from jupyterhub_services.handlers.base import BaseRequestHandler


def validate_reflect_token(token):
    match = re.match(r'[a-zA-Z0-9+/=]{32}', token)
    if not match:
        raise HTTPError(HTTPStatus.BAD_REQUEST, reason='Malformed token')


class ReflectorRequestHandler(HubOAuthenticated, BaseRequestHandler):

    @authenticated
    async def get(self, reflect_token):
        user = await self.get_user()
        username = user.get('name')

        validate_reflect_token(reflect_token)

        note = self.get_arguments('note')
        if note:
            note = note[0]
        else:
            note = 'hpc'

        token = await self.hub_api_request('POST', f'users/{username}/tokens',
                                           json={
                                               'note': note
                                           })

        pickled_token = pickle.dumps(token)
        await self.service.redis.publish(f'reflector-{reflect_token}', pickled_token)

        next = self.get_arguments('next')
        if next:
            self.redirect(next[0])
        else:
            self.write(token)


class ReflectorRetrieveHandler(BaseRequestHandler):

    async def get(self, reflect_token):
        key = f'reflector-{reflect_token}'

        validate_reflect_token(reflect_token)

        psub = self.service.redis.pubsub(ignore_subscribe_messages=True)

        async with psub as p:
            await p.subscribe(key)

            async for message in p.listen():
                if message.get('type') == 'message':
                    token = pickle.loads(message.get('data'))

                    self.write(token)
                    break

            await p.unsubscribe(key)
