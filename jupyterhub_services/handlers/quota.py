from jupyterhub.services.auth import HubOAuthCallbackHandler
from jupyterhub_services.handlers.base import BaseRequestHandler
from jupyterhub_services.share import Share

from datetime import datetime


class QuotaHandler(HubOAuthCallbackHandler, BaseRequestHandler):

    async def get(self, id):
        k = f'share-quota-{id}'

        with self.service.session() as session:
            # TODO: Check if used allowed to access share

            user = await self.get_user()

            try:
                quota = await self.service.cache.get(k)
                raise KeyError()
            except KeyError:
                share = session.query(Share).filter_by(id=id).one()

                quota = await share.get_quota(self.service, user['last_activity'])

                await self.service.cache.set(k, quota, 5*60)  # cache for 5 min

            self.finish({
                'quota': {
                    'available': quota[0],
                    'used': quota[1]
                }
            })
