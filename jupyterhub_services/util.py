import re
import secrets
import pickle
import logging
import requests
import asyncio
import datetime

from concurrent.futures import ThreadPoolExecutor

from jupyterhub.utils import url_path_join


class _AsyncRequests:
    """Wrapper around requests to return a Future from request methods
    A single thread is allocated to avoid blocking the IOLoop thread.
    """

    def __init__(self):
        self.executor = ThreadPoolExecutor(5, 'async_requests_')
        real_submit = self.executor.submit
        self.executor.submit = lambda *args, **kwargs: asyncio.wrap_future(
            real_submit(*args, **kwargs)
        )

    def __getattr__(self, name):
        requests_method = getattr(requests, name)
        return lambda *args, **kwargs: self.executor.submit(
            requests_method, *args, **kwargs
        )


async_requests = _AsyncRequests()


class RedisCache:

    def __init__(self, redis, **kwargs):
        self.redis = redis
        self.log = logging.getLogger('tornado.application')
        self.log.info('Initialized Redis cache')

    async def set(self, key, value, ttl):
        pickled_object = pickle.dumps(value)
        await self.redis.setex(key, ttl, pickled_object)

        self.log.info('Cache set: key=%s, ttl=%d', key, ttl)

    async def get(self, key):
        pickled_object = await self.redis.get(key)
        if not pickled_object:
            self.log.info('Cache miss: key=%s', key)
            raise KeyError()

        value = pickle.loads(pickled_object)
        self.log.info('Cache hit: key=%s', key)

        return value


def new_token():
    return secrets.token_hex(16)


async def api_request(method, endpoint, **kwargs):
    """Make an API request"""

    token = kwargs.pop('token')
    api_url = kwargs.pop('api_url')
    stream = kwargs.get('stream')
    headers = kwargs.setdefault('headers', {})
    headers.setdefault('Authorization', f'token {token}')
    url = url_path_join(api_url, endpoint)

    r = await async_requests.request(method, url, **kwargs)
    if stream:
        return r
    else:
        r.raise_for_status()

        return r.json()


parse_time_regex = re.compile(r'((?P<years>\d+?)y)?((?P<months>\d+?)M)?((?P<weeks>\d+?)w)?((?P<days>\d+?)d)?((?P<hours>\d+?)h)?((?P<minutes>\d+?)m)?((?P<seconds>\d+?)s)?')


def parse_timedelta(str):
    parts = parse_time_regex.match(str)
    if not parts:
        return

    parts = parts.groupdict()

    time = {}
    for (name, param) in parts.items():
        if param:
            time[name] = int(param)

    months = time.pop('months', 0)
    years = time.pop('years', 0)
    time['days'] = time.get('days', 0) + years * 365 + months * 31

    return datetime.timedelta(**time)


def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text  # or whatever
