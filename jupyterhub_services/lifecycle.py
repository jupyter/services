#!/bin/env python3

import os
import logging
import argparse
import jinja2
import asyncio
import pytz
import humanize
import dateutil.parser

from datetime import datetime
from smtplib import SMTP
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import kubernetes_asyncio as k8s

from jupyterhub_services.util import api_request, parse_timedelta

now = datetime.utcnow().replace(tzinfo=pytz.utc)


class User:

    def __init__(self, args, data):
        self.args = args
        self.data = data

        self.created = dateutil.parser.parse(data['created'])
        self.last_activity = dateutil.parser.parse(data['last_activity'])

        self.authstate = None

    @classmethod
    async def get_all(cls, args):
        resp = await api_request('GET', '/users',
                                 token=args.token,
                                 api_url=args.api_url)

        users = [cls(args, u) for u in resp]

        return users

    @classmethod
    async def get_all_quotas(cls, users):
        coros = [u.get_quota() for u in users]

        return asyncio.gather_all(coros)

    def __getattr__(self, key):
        if key in self.__dict__['data']:
            return self.__dict__['data'][key]

    @property
    def mail(self):
        return self.authstate.get('Mail')

    @property
    def is_special(self):
        return self.admin or 'manager' in self.groups or 'admin' in self.groups

    @property
    def age(self):
        return now - self.last_activity

    async def get_quota(self):
        self.quota = await api_request('GET', '/quota',
                                       api_url='https://jupyter.rwth-aachen.de'
                                       '/services/share/api')

        return self.quota

    async def _get_authstate(self):
        """Get a user auth state."""

        user = await api_request('GET', '/users/' + self.name,
                                 token=self.args.token,
                                 api_url=self.args.api_url)

        self.auth_state = user.get('auth_state')
        if not self.auth_state:
            raise RuntimeError('Failed to get user auth state')

    async def _send_mail(self, subject, template, ns={}, priority=False):
        ns.update({
            'user': self
        })

        sender = 'jupyter-admin@lists.rwth-aachen.de'
        receipient = self.mail

        tm = jinja2.Template(template)

        body_text = tm.render(**ns)
        body = MIMEText(body_text)

        msg = MIMEMultipart()

        msg['Subject'] = f'[RWTHjupyter] {subject}'
        msg['From'] = f'Jupyter Admins <{sender}>'

        if self.args.debug:
            msg['To'] = 'Steffen Vogel <svogel2@eonerc.rwth-aachen.de>'
            receivers = ['svogel2@eonerc.rwth-aachen.de']
        else:
            msg['To'] = [f'RWTHjupyter User <{receipient}>']
            receivers = [receipient]

        if priority:
            msg['X-Priority'] = '1'
            msg['X-MSMail-Priority'] = 'High'
            msg['Importance'] = 'High'

        msg.attach(body)

        with SMTP(self.args.smtp_server, port=self.args.smtp_port) as server:
            server.send_message(msg, sender, receivers)

    async def notify(self):
        """Notify the user via Email and a provided Jinja2 template"""

        logging.info('Notifying user %s which was last used %s (%s)',
                     self.name,
                     self.last_activity,
                     humanize.naturaltime(self.age))

        try:
            if not self.auth_state:
                await self._get_authstate()

            await self._send_mail('Account expiration', 'mails/deletion.txt',
                                  priority=True)
        except RuntimeError as e:
            logging.warning('%s for user %s' % (e, self.name))

    async def delete(self):
        """Delete user account from JupyterHub database."""

        logging.info('Deleting user %s which was last used %s (%s)',
                     self.name,
                     self.last_activity,
                     humanize.naturaltime(self.age))

        try:
            if not self.auth_state:
                await self._get_authstate()

            await self._send_mail('Account deletion', 'mails/deletion.txt')
        except RuntimeError as e:
            logging.warning('%s for user %s' % (e, self.name))

        # if not self.args.dry_run:
        #     await api_request('DELETE', '/users/' + self.name,
        #                       token=self.args.token,
        #                       api_url=self.args.api_url)

        # self.delete_pvc()

    async def delete_pvc(self):
        """Delete PersistentVolumeClaim from Kubernetes API."""

        core = k8s.client.CoreV1Api()
        await core.delete_namespaced_persistent_volume_claim(
            'claim-' + self.name,
            'jhub-users',
            dry_run=self.args.dry_run)

        logging.info('Deleted PVC of user %s', self.name)

    async def lifecycle(self):
        if self.age > self.args.deletion_age:
            if self.is_special:
                logging.info('Skipping deletion of special user %s', self.name)
            else:
                await self.delete()
        if self.age > self.args.notification_age:
            if self.is_special:
                logging.info('Skipping notification of special user %s',
                             self.name)
            else:
                await self.notify()


async def run():
    parser = argparse.ArgumentParser(
        description='Delete user accounts according to life-cycle')

    parser.add_argument('-N', '--notification-age',
                        type=parse_timedelta,
                        help='time before notification',
                        default=os.environ.get('NOTIFICATION', '1y'))
    parser.add_argument('-D', '--deletion-age',
                        type=parse_timedelta,
                        help='time before deletion',
                        default=os.environ.get('DELETION', '1y27w'))
    parser.add_argument('-t', '--token',
                        type=str,
                        help='JupyterHub token',
                        default=os.environ.get('JUPYTERHUB_API_TOKEN'))
    parser.add_argument('-a', '--api-url', type=str,
                        help='The URL of the JupyterHub API endpoint',
                        default=os.environ.get(
                            'JUPYTERHUB_API_URL',
                            'https://jupyter.rwth-aachen.de/hub/api'))
    parser.add_argument('-p', '--smtp-port',
                        type=int,
                        default=int(os.environ.get('SMTP_PORT', 25)))
    parser.add_argument('-s', '--smtp-server',
                        type=str,
                        default=os.environ.get(
                            'SMTP_HOST',
                            'smarthost.rwth-aachen,de'))
    parser.add_argument('-S', '--sender-email',
                        type=str,
                        default=os.environ.get(
                            'SMTP_SENDER_MAIL',
                            'jupyter-admin@lists.rwth-aachen.de'))
    parser.add_argument('-d', '--debug',
                        type=bool,
                        default=bool(os.environ.get('DEBUG', False)))
    parser.add_argument('-n', '--dry-run',
                        type=bool,
                        default=bool(os.environ.get('DRY_RUN', False)))

    args = parser.parse_args()

    level = logging.DEBUG if args.debug else logging.INFO

    logging.basicConfig(
        format='%(asctime)-15s [%(levelname)s] %(message)s',
        level=level
    )

    logging.debug("Logging level is %s", logging.getLevelName(level))

    if os.environ.get('KUBECONFIG'):
        await k8s.config.load_kube_config()
    else:
        k8s.config.load_incluster_config()

    users = await User.get_all(args)

    deletions = [u for u in users if
                 not u.is_special and u.age > args.deletion_age]
    notifications = [u for u in users
                     if not u.is_special and
                     args.deletion_age > u.age > args.notification_age]

    logging.info("Deleting %d users", len(deletions))
    logging.info("Notifying %d users", len(notifications))

    coros = [u.delete() for u in deletions] + [u.notify() for u in notifications]

    await asyncio.gather(*coros)


def main():
    asyncio.run(run())
