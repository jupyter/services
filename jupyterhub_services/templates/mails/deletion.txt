Dear RWTHjupyter user,

According to our terms of service we have deleted your account ({{ user.name }}) and the associated data after an inactivity of 6 months.

In case of questions, please contact the Service Desk of the RWTH IT Center at servicedesk@itc.rwth-aachen.de.

Best regards,
The RWTHjupyter admins


----
You are receiving this mail because you are a user of RWTHjupyter ({{ service.public_url }})