from jupyterhub_services.handlers.template import AuthenticatedTemplateHandler

from jupyterhub_services.service import Service
from jupyterhub_services.handlers.support import SupportDownloadHandler


class SupportService(Service):

    name = 'support'

    @property
    def handlers(self):
        return [
            (r'/', AuthenticatedTemplateHandler, {
                'template': 'support.html',
                'user_full': True,
                'user_progress': True
            }),
            (r'/(download|api)', SupportDownloadHandler)
        ]
