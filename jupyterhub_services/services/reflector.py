from jupyterhub_services.handlers.reflector import ReflectorRequestHandler
from jupyterhub_services.handlers.reflector import ReflectorRetrieveHandler

from jupyterhub_services.service import Service


class ReflectorService(Service):

    name = 'reflector'

    @property
    def handlers(self):
        return [
            (r'/api/request/(.+)', ReflectorRequestHandler),
            (r'/api/retrieve/(.+)', ReflectorRetrieveHandler)
        ]
