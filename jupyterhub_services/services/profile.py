import os
import gitlab
import json

from http import HTTPStatus
from tornado.web import RedirectHandler, HTTPError

from jupyterhub.utils import url_path_join
from jupyterhub_services.service import Service

from jupyterhub_services.handlers.gitlab import GitLabArtifactHandler
from jupyterhub_services.handlers.gitlab import GitLabBuildHandler
from jupyterhub_services.handlers.gitlab import GitLabVersionHandler
from jupyterhub_services.handlers.gitlab import GitLabHookHandler
from jupyterhub_services.handlers.profiles import ProfilesHandler
from jupyterhub_services.handlers.profiles import ProfileListHandler
from jupyterhub_services.handlers.profiles import ProfileDetailHandler
from jupyterhub_services.handlers.profiles import ProfileGraphHandler
from jupyterhub_services.handlers.profiles import ProfileFormHandler
from jupyterhub_services.handlers.profiles import SPDXLicenseHandler
from jupyterhub_services.handlers.template import AuthenticatedTemplateHandler

from traitlets import Int, Unicode, default


class ProfileService(Service):

    name = 'profile'

    # Gitlab
    gitlab_token = Unicode()
    gitlab_project_id = Int()
    gitlab_instance = Unicode()
    gitlab_job_search_depth = Int()
    gitlab_webhook_token = Unicode()

    @default('gitlab_webhook_token')
    def _default_gitlab_webhook_token(self):
        return os.environ.get('GITLAB_WEBHOOK_TOKEN')

    @default('gitlab_token')
    def _default_gitlab_token(self):
        return os.environ.get('GITLAB_TOKEN')

    @default('gitlab_project_id')
    def _default_gitlab_project_id(self):
        return os.environ.get('GITLAB_PROJECT_ID', 42490)  # jupyter/profiles

    @default('gitlab_instance')
    def _default_gitlab_instance(self):
        return os.environ.get('GITLAB_INSTANCE',
                              'https://git.rwth-aachen.de')

    @default('gitlab_job_search_depth')
    def _default_gitlab_job_search_depth(self):
        return os.environ.get('GITLAB_JOBS_SEARCH_DEPTH', 200)

    def __init__(self):
        super().__init__()

        self._check_config()

    async def init(self):
        await super().init()

        await self._init_gitlab()

    async def _init_gitlab(self):
        self.gitlab = gitlab.Gitlab(self.gitlab_instance,
                                    private_token=self.gitlab_token)

        if self.debug:
            self.gitlab.enable_debug()

        self.gitlab.auth()

        self.project = self.gitlab.projects.get(self.gitlab_project_id)

        # Pre-warm cache
        profiles = await self.get_profiles()
        self.log.info('Found %d profiles', len(profiles))

    def _check_config(self):
        if not self.gitlab_project_id:
            raise RuntimeError(
                'Project ID missing. Please define GITLAB_PROJECT_ID environment variable')
        if not self.gitlab_instance:
            raise RuntimeError(
                'Instance missing. Please define GITLAB_INSTANCE environment variable')

    def get_last_job(self, name, status=None):
        jobs = self.project.jobs.list(as_list=False)

        i = 0
        for j in jobs:
            i = i + 1
            if i > self.gitlab_job_search_depth:
                break

            if j.name == name and (status is None or j.status == status) and j.ref == 'master':
                return j

    async def get_last_build(self, name, status=None, ttl=60*60, cached=True):
        k = f'last-{status}-build-{name}' if status else \
            f'last-build-{name}'

        if cached:
            try:
                return await self.cache.get(k)
            except KeyError:
                pass

        job = self.get_last_job(name, status=status)
        if job is None:
            return None

        keys = ['id', 'status', 'created_at', 'started_at', 'finished_at', 'duration', 'web_url', 'commit', 'user']
        attrs = {k: v for k, v in job.attributes.items() if k in keys}

        await self.cache.set(k, attrs, ttl)

        return attrs

    async def get_artifact(self, job, path, ttl=60*60, cached=True):
        k = f'artifacts-{job}-{path}'

        if cached:
            try:
                return await self.cache.get(k)
            except KeyError:
                pass

        build = await self.get_last_build(job, status='success', ttl=ttl, cached=cached)
        if not build:
            return None

        job = self.project.jobs.get(build['id'])
        art = job.artifact(path)

        await self.cache.set(k, art, ttl)

        return art

    async def get_versions(self, slug, **kwargs):
        versions = {}
        for repo in ['conda', 'pip', 'jupyter-labextensions', 'jupyter-serverextensions', 'jupyter-kernelspecs']:
            try:
                version = await self.get_artifact(slug, f'versions/{repo}.json', **kwargs)
                version = json.loads(version)

                if version:
                    versions[repo] = version
            except Exception:
                pass

        return versions

    async def get_profiles(self, **kwargs):
        if 'ttl' not in kwargs:
            kwargs['ttl'] = 5*60

        pl = await self.get_artifact('generate', 'profile-definitions.json',
                               **kwargs)
        if pl is None:
            raise HTTPError(HTTPStatus.NOT_FOUND, reason='Artifact not found')

        return json.loads(pl)

    async def get_profile(self, slug):
        pl = await self.get_profiles()

        return [p for p in pl if p['slug'] == slug][0]

    @property
    def handlers(self):
        return [
            (r'/api/hook', GitLabHookHandler),
            (r'/api/([^/]+)/build', GitLabBuildHandler),
            (r'/api/([^/]+)/versions/([^/]+)', GitLabVersionHandler),
            (r'/api/artifacts/([^/]+)/([^/]+)', GitLabArtifactHandler),
            (r'/api/licenses/spdx', SPDXLicenseHandler),
            (r'/api(?:/([^/]+))?', ProfilesHandler),

            (r'/', RedirectHandler, {
                'url': url_path_join(self.prefix, 'build')
            }),
            (r'/(build)', AuthenticatedTemplateHandler, {
                'template': 'profile-build.html'
            }),
            (r'/(list)', ProfileListHandler),
            (r'/(details)/([^/]+)', ProfileDetailHandler),
            (r'/(create)', ProfileFormHandler),
            (r'/graph.svg', ProfileGraphHandler)
        ]
