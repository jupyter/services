
from .announcement import AnnouncementService
from .coupon import CouponService
from .profile import ProfileService
from .share import ShareService
from .submission import SubmissionService
from .whoami import WhoAmIService
from .survey import SurveyService
from .link import LinkService
from .support import SupportService
from .statistics import StatisticsService
from .reflector import ReflectorService

services = {
    AnnouncementService,
    CouponService,
    ProfileService,
    ShareService,
    SubmissionService,
    WhoAmIService,
    LinkService,
    SurveyService,
    SupportService,
    StatisticsService,
    ReflectorService
}

_services_map = {service.name: service for service in services}


def by_name(name):
    return _services_map[name]
