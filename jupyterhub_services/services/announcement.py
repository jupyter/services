from jupyterhub_services.handlers.announcement import AnnouncementHandler
from jupyterhub_services.handlers.template import AuthenticatedTemplateHandler

from jupyterhub_services.service import Service


class AnnouncementService(Service):

    name = 'announcement'

    @property
    def handlers(self):
        return [
            (r'/', AuthenticatedTemplateHandler, {
                'template': 'announcement.html'
            }),
            (r'/api(?:/(\d+))?', AnnouncementHandler),
        ]
