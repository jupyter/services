from jupyterhub_services.handlers.coupons import CouponCreationHandler
from jupyterhub_services.handlers.coupons import CouponRedemptionHandler
from jupyterhub_services.handlers.coupons import CouponTemplateHandler

from jupyterhub_services.service import Service


class CouponService(Service):

    name = 'coupon'

    @property
    def handlers(self):
        return [
            (r'/api/redeem/([^/]+)', CouponRedemptionHandler),
            (r'/api/create', CouponCreationHandler),
            (r'/', CouponTemplateHandler)
        ]
