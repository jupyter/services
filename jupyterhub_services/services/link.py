from jupyterhub_services.service import Service
from jupyterhub_services.handlers.template import TemplateHandler


class LinkService(Service):

    name = 'link'

    @property
    def handlers(self):
        return [
            (r'/', TemplateHandler, {
                'template': 'link.html'
            }),
        ]
