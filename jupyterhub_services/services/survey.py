from jupyterhub_services.handlers.surveys import SurveyCompletionHandler, SurveyHandler

from jupyterhub_services.service import Service


class SurveyService(Service):

    name = 'survey'

    @property
    def handlers(self):
        return [
            (r'/completed/(\d+)', SurveyCompletionHandler),
            (r'/', SurveyHandler)
        ]
