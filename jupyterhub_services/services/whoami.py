import re
import datetime
from jupyterhub_services.handlers.whoami import WhoAmIHandler, WhoAmIAPIHandler

from jupyterhub_services.service import Service


class WhoAmIService(Service):

    name = 'whoami'

    @property
    def handlers(self):
        return [
            (r'/api/([a-z0-9@]*)', WhoAmIAPIHandler),
            (r'/([a-z0-9@]*)', WhoAmIHandler)
        ]

    async def get_moodle_course_details(self, handler, courses):
        if len(courses) == 0:
            return

        course_ids = [c['course_id'] for c in courses]

        k = 'moodle-course-details-' + '-'.join(map(str, course_ids))

        try:
            course_details = await self.cache.get(k)
        except KeyError:
            params = {f'options[ids][{i}]': c for i, c in enumerate(course_ids)}  # noqa E501
            course_details = await handler.moodle_api_request('core_course_get_courses', params)  # noqa E501

            await self.cache.set(k, course_details, 60*60)

        course_map = {}
        for course in courses:
            course_map[course['course_id']] = course

        for course_detail in course_details:
            course_id = course_detail['id']
            course = course_map[course_id]

            langs = ['en', 'de']
            matches = re.finditer(r'([^\[\]]+?)(?: \(([^\)]+)\))? \[(\d{2,}\.\d{2,})\]', course_detail['fullname'])  # noqa E501
            for lang, match in zip(langs, matches):
                course_detail[f'fullname_{lang}'] = match.group(1)
                course_detail['type'] = match.group(2)

                if match.group(3):
                    course_detail['lvid'] = match.group(3)

            for date in ['startdate', 'enddate']:
                if date in course_detail:
                    course_detail[date] = datetime.datetime.fromtimestamp(course_detail[date])  # noqa E501

            course.update(course_detail)

            del course['course_id']

        for course in courses:
            if 'id' not in course:
                course['id'] = course.pop('course_id')
