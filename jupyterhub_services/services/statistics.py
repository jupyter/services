from jupyterhub_services.service import Service
from jupyterhub_services.handlers.template import TemplateHandler, AuthenticatedTemplateHandler

from tornado.web import RedirectHandler
from jupyterhub.utils import url_path_join


class StatisticsService(Service):

    name = 'statistics'

    @property
    def handlers(self):
        return [
            (r'/', RedirectHandler, {
                'url': url_path_join(self.prefix, 'jupyterhub')
            }),
            (r'/(user)(?:/([a-z0-9-]+))?', AuthenticatedTemplateHandler, {
                'template': 'statistics.html'
            }),
            (r'/([a-z0-9-]*)', TemplateHandler, {
                'template': 'statistics.html'
            })
        ]
