import os
from traitlets import Unicode, default

from jupyterhub_services.handlers.shares import ShareHandler
from jupyterhub_services.handlers.template import AuthenticatedTemplateHandler
from jupyterhub_services.handlers.quota import QuotaHandler
from jupyterhub_services.handlers.sciebo import ScieboOAuthCallbackHandler
from jupyterhub_services.handlers.sciebo import ScieboEndpointHandler

from jupyterhub_services.service import Service

import kubernetes_asyncio as k8s


class ShareService(Service):

    name = 'share'

    ceph_mon_host = Unicode()
    ceph_key = Unicode()
    ceph_client = Unicode()

    prometheus_url = Unicode()

    @default('ceph_mon_host')
    def _default_ceph_mon_host(self):
        return os.environ['CEPH_MON_HOST']

    @default('ceph_key')
    def _default_ceph_key(self):
        return os.environ['CEPH_KEY']

    @default('ceph_client')
    def _default_ceph_client(self):
        return os.environ.get('CEPH_CLIENT', 'client.service-share')

    @default('prometheus_url')
    def _default_prometheus_url(self):
        return os.environ['PROMETHEUS_URL']

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    async def init(self):
        await super().init()

        if os.environ.get('KUBECONFIG'):
            await k8s.config.load_kube_config()
        else:
            k8s.config.load_incluster_config()

    @property
    def handlers(self):
        return [
            (r'/', AuthenticatedTemplateHandler, {
                'template': 'share.html'
            }),
            (r'/sciebo', AuthenticatedTemplateHandler, {
                'template': 'sciebo.html'
            }),
            (r'/api/sciebo/endpoints', ScieboEndpointHandler),
            (r'/api/oauth_callback', ScieboOAuthCallbackHandler),
            (r'/api(?:/([^/]+))?', ShareHandler),
            (r'/api/([0-9]+)/quota', QuotaHandler),
        ]
