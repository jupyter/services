from jupyterhub_services.handlers.submissions import SubmissionHandler

from jupyterhub_services.service import Service


class SubmissionService(Service):

    name = 'submission'

    @property
    def handlers(self):
        return [
            (r'/([a-z0-9-]+)/([a-z0-9_-]+)', SubmissionHandler),
            (r'/([a-z0-9-]+)/([a-z0-9_-]+)/([a-z0-9]+)', SubmissionHandler),
            (r'/([a-z0-9-]+)/([a-z0-9_-]+)/([a-z0-9]+)/([0-9]+)', SubmissionHandler)
        ]
