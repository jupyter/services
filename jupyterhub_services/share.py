import requests
from tornado.web import HTTPError
import kubernetes_asyncio as k8s
import kubernetes_asyncio.client.models as km
from lxml import etree
from sqlalchemy import orm
from datetime import datetime
from lxml import etree
import string
import random

from jupyterhub_services.orm import Share as OrmShare
from jupyterhub_services.util import async_requests
from jupyterhub_services.ceph import CephfsPVC


class Share(OrmShare):

    namespace = 'jhub-test'

    def __init__(self, name, mountpoint, created_by, **kwargs):
        self.name = name
        self.mountpoint = mountpoint
        self.created_by = created_by
        self.config = kwargs

        self.init_on_load()

    @orm.reconstructor
    def init_on_load(self):
        self.core = k8s.client.CoreV1Api()

    @property
    def k8s_name(self):
        return f'jupyter-share-{self.id}'

    @property
    def labels(self):
        return {
            'app': 'service-share',
            'user': self.created_by
        }

    def validate(self):
        return True

    async def get_quota(self, service, last_activity=None):
        return None, None  # not implemented

    @staticmethod
    def class_from_type(type):
        if type == 'webdav':
            return WebDAVShare
        elif type == 'sciebo':
            return ScieboShare
        elif type == 'pvc':
            return PVCShare


class PVCShare(Share):

    MAX_CAPACITY = 25  # GiB
    STORAGE_CLASS = 'rook-cephfs'

    __mapper_args__ = {
        'polymorphic_identity': 'pvc'
    }

    def __init__(self, mountpoint, created_by, **kwargs):
        capacity = kwargs.get('capacity')
        if capacity > self.MAX_CAPACITY:
            raise Exception('Maximum storage quota exceeded')

        super(Share).__init__(mountpoint, created_by, **kwargs)

    async def query_prometheus(self, metric, service, last_activity):
        resp = await async_requests.get(service.prometheus_url + '/api/v1/query', params={
                'query': metric + '{'
                            'persistentvolumeclaim="' + self.k8s_name + '",'
                            'exported_namespace="' + self.namespace + '"'
                          '}',
                'time': last_activity.isoformat()
            })

        resp = resp.json()

        data = resp.get('data')
        result = data.get('result')
        value = result[0].get('value')

        return datetime.fromtimestamp(value[0]), value[1]

    async def get_quota(self, service, last_activity=None):
        pvc = await self.core.read_namespaced_persistent_volume_claim_status(self.k8s_name, self.namespace)  # noqa E501

        if pvc.status.phase != 'Bound':
            raise RuntimeError('PVC not bound')

        storage_class = pvc.spec.storage_class_name
        if storage_class != self.STORAGE_CLASS:
            raise RuntimeError('Invalid storage class: ' + storage_class)

        provisioner = pvc.metadata.annotations['volume.beta.kubernetes.io/storage-provisioner']  # noqa E501
        if provisioner != 'rook-ceph.cephfs.csi.ceph.com':
            raise RuntimeError('Invalid storage provisioner: ' + provisioner)

        pv = await self.core.read_persistent_volume(pvc.spec.volume_name)

        if pv.spec.csi.driver != provisioner:
            raise RuntimeError('Invalid CSI driver: ' + pv.spec.csi.driver)

        if True:
            vol = CephfsPVC(self.k8s_name, self.namespace, service)

            return await vol.get_quota()
        else:
            ts_used_bytes, used_bytes = await self.query_prometheus('kubelet_volume_stats_used_bytes', service, last_activity)
            ts_capacity_bytes, capacity_bytes = await self.query_prometheus('kubelet_volume_stats_capacity_bytes', service, last_activity)

            return [
                int(capacity_bytes),
                int(used_bytes)
            ]

    async def create_k8s(self):
        pvc = self._construct_k8s_pvc()

        await self.core.create_namespaced_persistent_volume_claim(self.namespace, pvc)  # noqa E501

    async def delete_k8s(self):
        try:
            await self.core.delete_namespaced_persistent_volume_claim(self.k8s_name, self.namespace)  # noqa E501
        except k8s.client.exceptions.ApiException as e:
            if e.status != 404:
                raise

    def _construct_k8s_pvc(self):
        return km.V1PersistentVolumeClaim(
            metadata=km.V1ObjectMeta(
                labels=self.labels,
                name=self.k8s_name
            ),
            spec=km.V1PersistentVolumeClaimSpec(
                access_modes=['ReadWriteMany'],
                resources=km.V1ResourceRequirements(
                    requests={
                        'storage': f'{self.capacity}Gi'
                    }
                ),
                storage_class_name=self.STORAGE_CLASS
            )
        )


class WebDAVShare(Share):

    __mapper_args__ = {
        'polymorphic_identity': 'webdav'
    }

    @property
    def server(self):
        return self.config.get('server')

    @property
    def share(self):
        return self.config.get('share')

    @property
    def username(self):
        return self.config.get('username')

    @property
    def password(self):
        return self.config.get('password')

    def validate(self):
        return True

    async def create_k8s(self):
        secret = self._construct_k8s_secret()
        pv = self._construct_k8s_pv()
        pvc = self._construct_k8s_pvc()

        await self.core.create_namespaced_persistent_volume_claim(self.namespace, pvc)  # noqa E501
        await self.core.create_namespaced_secret(self.namespace, secret)
        await self.core.create_persistent_volume(pv)

    async def delete_k8s(self):
        try:
            await self.core.delete_persistent_volume(self.k8s_name)
        except k8s.client.exceptions.ApiException as e:
            if e.status != 404:
                raise

        try:
            await self.core.delete_namespaced_secret(self.k8s_name, self.namespace)
        except k8s.client.exceptions.ApiException as e:
            if e.status != 404:
                raise

        try:
            await self.core.delete_namespaced_persistent_volume_claim(self.k8s_name, self.namespace)  # noqa E501
        except k8s.client.exceptions.ApiException as e:
            if e.status != 404:
                raise

    async def get_quota(self, service, last_activity=None):
        url = self.server + self.share

        et = etree.Element('{DAV:}propfind')
        props = etree.SubElement(et, '{DAV:}prop')
        available = etree.SubElement(props, '{DAV:}quota-available-bytes')
        used = etree.SubElement(props, '{DAV:}quota-used-bytes')

        xml_str = etree.tostring(et, encoding='utf8', method='xml')

        response = await async_requests.request('PROPFIND', url,
                                                auth=(self.username,
                                                      self.password),
                                                headers={
                                                    'Depth': '0',
                                                    'Content-Type': 'text/xml'
                                                },
                                                data=xml_str)
        response.raise_for_status()

        root = etree.fromstring(response.content)
        prop_available = root.find('.//{DAV:}quota-available-bytes')
        prop_used = root.find('.//{DAV:}quota-used-bytes')

        available = int(prop_available.text) if prop_available is not None else None  # noqa E501
        used = int(prop_used.text) if prop_used is not None else None

        if available:
            return available + used, used
        else:
            return None, used

    def _construct_k8s_secret(self):
        return km.V1Secret(
            metadata=km.V1ObjectMeta(
                name=self.k8s_name,
                labels=self.labels
            ),
            type='Opaque',
            string_data={
                'username': self.username,
                'password': self.password
            }
        )

    def _construct_k8s_pv(self):
        return km.V1PersistentVolume(
            metadata=km.V1ObjectMeta(
                labels=self.labels,
                name=self.k8s_name
            ),
            spec=km.V1PersistentVolumeSpec(
                access_modes=['ReadWriteMany'],
                capacity={
                    'storage': '1Gi'
                },
                csi=km.V1CSIPersistentVolumeSource(
                    driver='csi-davfsplugin',
                    node_publish_secret_ref=km.V1SecretReference(
                        name=self.k8s_name,
                        namespace=self.namespace
                    ),
                    volume_attributes={
                        'server': self.server,
                        'share': self.share
                    },
                    volume_handle=self.k8s_name
                ),
                mount_options=[
                    'uid=1000',
                    'gid=100'
                ],
                persistent_volume_reclaim_policy='Retain',
            )
        )

    def _construct_k8s_pvc(self):
        return km.V1PersistentVolumeClaim(
            metadata=km.V1ObjectMeta(
                labels=self.labels,
                name=self.name
            ),
            spec=km.V1PersistentVolumeClaimSpec(
                storage_class_name='',
                resources=km.V1ResourceRequirements(
                    requests={
                        'storage': '1Gi'
                    }
                ),
                access_modes=['ReadWriteMany'],
                volume_name=self.name
            )
        )


class ScieboShare(WebDAVShare):

    share_api = 'ocs/v1.php/apps/files_sharing/api/v1/shares'

    __mapper_args__ = {
        'polymorphic_identity': 'sciebo'
    }

    @property
    def share_id(self):
        return self.config.get('share_id')

    @property
    def share(self):
        return self.server + '/public.php/webdav'

    @property
    def password(self):
        return self.config.get('token')

    @staticmethod
    def generate_password(size=16, chars=string.ascii_uppercase + string.digits):
        return ''.join(random.choice(chars) for _ in range(size))

    # https://doc.owncloud.com/server/developer_manual/core/apis/ocs-share-api.html

    async def update(self, token, **kwargs):
        r = await async_requests.put(f'{self.server}/{self.share_api}/{self.share_id}',
                                     headers={
                                         'Authorization': f'Bearer {token}',
                                     },
                                     data=kwargs)
        r.raise_for_status()

    async def delete(self, token):
        r = await async_requests.delete(f'{self.server}/{self.share_api}/{self.share_id}',
                                        headers={
                                            'Authorization': f'Bearer {token}',
                                        })
        r.raise_for_status()

    async def create(self, token):

        r = await async_requests.post(f'{self.server}/{self.share_api}',
                                      headers={
                                          'Authorization': f'Bearer {token}',
                                      },
                                      data={
                                          'name': self.name,
                                          'shareType': 3, # public link
                                          'publicUpload': True,
                                          'path': self.config.get('path', '/'),
                                          'password': self.config.get('password', self.generate_password()),
                                          'permissions': self.config.get('permissions', 31)
                                      })
        print(r.content)
        r.raise_for_status()

        ocs = etree.XML(r.content)

        meta = ocs.find('meta')
        status = meta.find('status')

        if status.text == 'failure':
            code = int(meta.find('statuscode').text)
            message = meta.find('message').text

            raise HTTPError(code, reason=message)

        data = ocs.find('data')

        self.config['share_id'] = int(data.find('id').text)
        self.config['token'] = data.find('token').text

        # <?xml version="1.0"?>
        # <ocs>
        #   <meta>
        #     <status>ok</status>
        #     <statuscode>100</statuscode>
        #     <message/>
        #   </meta>
        #   <data>
        #     <id>115468</id>
        #     <share_type>3</share_type>
        #     <uid_owner>auser</uid_owner>
        #     <displayname_owner>A User</displayname_owner>
        #     <permissions>1</permissions>
        #     <stime>1481537775</stime>
        #     <parent/>
        #     <expiration/>
        #     <token>MMqyHrR0GTepo4B</token>
        #     <uid_file_owner>auser</uid_file_owner>
        #     <displayname_file_owner>A User</displayname_file_owner>
        #     <path>/Photos/Paris.jpg</path>
        #     <item_type>file</item_type>
        #     <mimetype>image/jpeg</mimetype>
        #     <storage_id>home::auser</storage_id>
        #     <storage>993</storage>
        #     <item_source>3994486</item_source>
        #     <file_source>3994486</file_source>
        #     <file_parent>3994485</file_parent>
        #     <file_target>/Shared/Paris.jpg</file_target>
        #     <share_with/>
        #     <share_with_displayname/>
        #     <url>https://your.owncloud.install.com/owncloud/index.php/s/MMqyHrR0GTepo4B</url>
        #     <mail_send>0</mail_send>
        #     <name>paris photo</name>
        #   </data>
        # </ocs>
