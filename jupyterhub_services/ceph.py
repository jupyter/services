import os
import asyncio
import configparser as cp
import threading
import logging
from concurrent.futures import ThreadPoolExecutor
import kubernetes_asyncio as k8s

# A global ThreadPoolExecutor singleton
tpe = None


class CephfsPVC:

    def __init__(self, name, namespace, svc):
        self.name = name
        self.namespace = namespace
        self.core = k8s.client.CoreV1Api()

        self.ceph_mon_host = svc.ceph_mon_host
        self.ceph_client = svc.ceph_client
        self.ceph_key = svc.ceph_key

        global tpe
        if tpe is None:
            tpe = ThreadPoolExecutor(10, 'rados_client')  # noqa E501

    def _init_rados(self):
        import rados

        # Safe Rados connection in TLS
        tls = threading.local()

        try:
            return tls.rados
        except AttributeError:

            ct = threading.current_thread()
            log = logging.getLogger(ct.name)
            log.info('Started Rados worker thread')

            tls.rados = rados.Rados(name=self.ceph_client,
                                    conf={
                                        'key': self.ceph_key,
                                        'mon_host': self.ceph_mon_host
                                    })
            tls.rados.connect()

            return tls.rados

    async def _get_subvol_name(self):
        pvc = await self.core.read_namespaced_persistent_volume_claim_status(
            self.name, self.namespace)
        pv = await self.core.read_persistent_volume(pvc.spec.volume_name)

        return pv.spec.csi.volume_attributes.get('subvolumeName')

    def _get_quota(self):
        import cephfs

        # Init per-thread singleton Rados connection
        rados = self._init_rados()

        with cephfs.LibCephFS(rados_inst=rados) as fs:
            path = os.path.join('/volumes/csi', self.subvol_name)

            mf = os.path.join(path, '.meta')
            fd = fs.open(mf, 'r', 0)
            b = fs.read(fd, 0, 1024).decode('utf-8')

            m = cp.ConfigParser()
            m.read_string(b)

            subdir = m.get('GLOBAL', 'path')
            path_full = os.path.join(path, subdir)

            try:
                rbytes_raw = fs.getxattr(path_full, 'ceph.dir.rbytes')
                rbytes = int(rbytes_raw)
            except cephfs.NoData:
                rbytes = None

            try:
                max_bytes_raw = fs.getxattr(path_full, 'ceph.quota.max_bytes')
                max_bytes = int(max_bytes_raw)
            except cephfs.NoData:
                max_bytes = None

        return max_bytes, rbytes

    async def get_quota(self):
        loop = asyncio.get_event_loop()

        self.subvol_name = await self._get_subvol_name()

        return await loop.run_in_executor(tpe, self._get_quota)
