import sys
from jupyterhub_services.services import by_name


def main():
    if len(sys.argv) != 2:
        sys.stderr.write(f'usage: {sys.argv[0]} SVC_NAME')
        sys.exit(-1)

    svc_name = sys.argv[1]
    svc_cls = by_name(svc_name)

    svc = svc_cls()
    svc.start()
