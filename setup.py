from setuptools import setup, find_packages

setup(
    name='jupyterhub-services',
    description='Several JupyterHub services used by RWTHs Jupyter Cluster',
    version='0.1',
    packages=find_packages(),
    author='Steffen Vogel',
    author_email='post@steffenvogel.de',
    keywords='docker jupyterhub jupyter',
    url='https://git.rwth-aachen.de/jupyter/services',
    entry_points={
        'console_scripts': [
            'jupyterhub-service = jupyterhub_services.app:main',
            'jupyterhub-lifecycle = jupyterhub_services.lifecycle:main'
        ],
    },
    install_requires=[
        'python-gitlab',
        'python-dateutil',
        'python-magic',
        'tornado',
        'SQLAlchemy',
        'slugify',
        'psycopg2',
        'jinja2',
        'requests',
        'jupyterhub',
        'kubernetes_asyncio',
        'humanize',
        'pytz',
        'beautifulsoup4',
        'lxml',
        'spdx-license-list',
        'aioredis @ git+https://github.com/aio-libs/aioredis-py.git@bb0dcebed350a5f764dc84c5b6bcb44a3bf98c6b'
    ],
    python_requires='>=3.5',
    include_package_data=True,
    zip_safe=False
)
