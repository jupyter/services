#!/bin/bash

KCTL="kubectl --context=rwth-jupyter-rancher"

JUPYTERHUB_SERVICE_NAME="whoami"

export JUPYTERHUB_API_TOKEN="$(${KCTL} -n jhub get secret service-token-${JUPYTERHUB_SERVICE_NAME} -o jsonpath='{.data.hub-token}' | base64 -d)"
export DRY_RUN=true
export DEBUG=true

export NOTIFICATION=60d
export DELETION=90d

jupyterhub-lifecycle $*
