
# We need Python 3.7 because python3-cephfs includes a native extension
# which we only have for this version
FROM python:3.7-buster

RUN echo "deb https://download.ceph.com/debian-15.2.8/ buster main" >> /etc/apt/sources.list
RUN wget -q -O- 'https://download.ceph.com/keys/release.asc' | apt-key add -

RUN apt-get update && \
    apt-get -y install \
        git \
        libpq-dev \
        python3-cephfs

RUN mkdir /src
WORKDIR /src

ADD requirements.txt /src
RUN python -m pip install -r requirements.txt

ADD . /src
RUN python setup.py install

ENV JUPYTERHUB_SERVICE_NAME announcement
ENV PYTHONPATH=/usr/lib/python3/dist-packages/

CMD jupyterhub-service ${JUPYTERHUB_SERVICE_NAME}
