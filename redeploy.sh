#!/bin/bash

set -e

SELECTORS="-l component=services"

if (( $# >= 1 )); then
	SELECTORS+=",service=$1"
fi

docker build --pull --tag registry.git.rwth-aachen.de/jupyter/provisioning/services .
docker push registry.git.rwth-aachen.de/jupyter/provisioning/services

kubectl -n jhub delete pod ${SELECTORS}
