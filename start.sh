#!/bin/bash

PID_FILE_POSTGRES=.kubectl_port_forward_postgres.pid
PID_FILE_PROMETHEUS=.kubectl_port_forward_prometheus.pid
PID_FILE_REDIS=.kubectl_port_forward_redis.pid

KCTL="kubectl --context=rwth-jupyter-rancher"

function stop_port_forwarding {
	for PID_FILE in ${PID_FILE_POSTGRES} ${PID_FILE_REDIS} ${PID_FILE_PROMETHEUS}; do
		if [ -f ${PID_FILE} ]; then
			PID=$(cat ${PID_FILE})
			kill ${PID}
			wait ${PID}
			rm ${PID_FILE}
		fi
	done
}

function finish {
	stop_port_forwarding
}

trap finish EXIT

if ! command -v kubectl &> /dev/null; then
    echo "kubectl not found, please install it"
    exit 1
fi

if ! command -v shyaml &> /dev/null; then
    echo "shyaml not found, please install it: pip install shyaml"
    exit 1
fi

export JUPYTERHUB_SERVICE_NAME=whoami
export JUPYTERHUB_HOST="https://jupyter.rwth-aachen.de"
export JUPYTERHUB_API_URL="${JUPYTERHUB_HOST}/hub/api"

if (( $# >= 1 )); then
	export JUPYTERHUB_SERVICE_NAME=$1
fi

export JUPYTERHUB_API_TOKEN="$(${KCTL} -n jhub get secret service-token-${JUPYTERHUB_SERVICE_NAME} -o jsonpath='{.data.hub-token}' | base64 -d)"
export DB_URL="$(${KCTL} -n jhub get secret service-token-${JUPYTERHUB_SERVICE_NAME} -o jsonpath='{.data.db-url}' | base64 -d | sed -e 's/postgres-postgresql.services/localhost/g')"
export REDIS_URL="$(${KCTL} -n jhub get secret service-token-${JUPYTERHUB_SERVICE_NAME} -o jsonpath='{.data.redis-url}' | base64 -d | sed -e 's/redis-master.services/localhost/g')"
export MOODLE_TOKEN="$(${KCTL} -n jhub get secret service-token-whoami -o jsonpath='{.data.moodle-token}' | base64 -d)"
export SCIEBO_CLIENT_ID="$(${KCTL} -n jhub get secret service-token-share -o jsonpath='{.data.sciebo-client-id}' | base64 -d)"
export SCIEBO_CLIENT_SECRET"$(${KCTL} -n jhub get secret service-token-share -o jsonpath='{.data.sciebo-client-secret}' | base64 -d)"
export GITLAB_TOKEN="$(${KCTL} -n jhub get secret service-token-${JUPYTERHUB_SERVICE_NAME} -o jsonpath='{.data.gitlab-token}' | base64 -d)"
export GITLAB_WEBHOOK_TOKEN="$(${KCTL} -n jhub get secret service-token-${JUPYTERHUB_SERVICE_NAME} -o jsonpath='{.data.gitlab-webhook-token}' | base64 -d)"
export PROMETHEUS_URL="http://localhost:8080"

echo "=== Configuration"
echo "  JUPYTERHUB_API_TOKEN=${JUPYTERHUB_API_TOKEN}"
echo "  DB_URL=${DB_URL}"
echo "  REDIS_URL=${REDIS_URL}"
echo "  GITLAB_TOKEN=${GITLAB_TOKEN}"
echo "  MOODLE_TOKEN=${MOODLE_TOKEN}"
echo "  SCIEBO_CLIENT_ID=${SCIEBO_CLIENT_ID}"
echo "  SCIEBO_CLIENT_SECRET=${SCIEBO_CLIENT_SECRET}"
echo "  GITLAB_WEBHOOK_TOKEN=${GITLAB_WEBHOOK_TOKEN}"

echo "=== Shutdown existing port-forwards"
stop_port_forwarding # Stop if still running...

echo "=== Setup new port-forwards"
${KCTL} --namespace=services \
    port-forward service/postgres-postgresql 5432:5432 &
echo $! > ${PID_FILE_POSTGRES}

${KCTL} --namespace=services \
    port-forward service/redis-master 6379:6379 &
echo $! > ${PID_FILE_REDIS}

${KCTL} --namespace=cattle-prometheus \
    port-forward service/access-prometheus 8080:80 &
echo $! > ${PID_FILE_PROMETHEUS}

export DEBUG=true

echo "=== Start service: ${JUPYTERHUB_SERVICE_NAME}"
echo " Running at: http://localhost:5000/services/${JUPYTERHUB_SERVICE_NAME}/"
jupyterhub-service ${JUPYTERHUB_SERVICE_NAME}
